package com.belushkin.lesson6.hospital.hospital;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.mockito.Matchers;

import java.util.Arrays;

import static org.junit.Assert.*;

public class BasicHospitalTest {

    @Test
    public void check_when_hospital_exist() {
        //given
        Hospital[] hospitals = new Hospital[5];
        //when
        for (int i = 0; i < hospitals.length; i++) {
            hospitals[i] = new CardioHospital();
        }
        //then
        for (Hospital hospital: hospitals) {
            assertNotNull(hospital.getIllness());
            assertThat(hospital.getIllness(), CoreMatchers.notNullValue());
            assertTrue(hospital.getIllness().length > 0);
            assertTrue(hospital.getEmployeesCount() > 0);
            assertNotEquals("", hospital.getName());
        }
    }

}
