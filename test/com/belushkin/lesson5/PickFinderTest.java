package com.belushkin.lesson5;

import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class PickFinderTest {

    @Test
    public void pickSum_should_getNoResult_when_noPicks() {
        // given
        int[] picks = {1};
        PickSearcher pickSearcher = mock(PickSearcher.class);
        when(pickSearcher.search(1)).thenReturn("this cool pick");
        when(pickSearcher.search(2)).thenReturn("this another pick");
        PickFinder pickFinder = new PickFinder(pickSearcher);
        // when
        int result = pickFinder.pickSum(picks);
        // then
        int expected = -1;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void pickSum_should_getSumResult_when_PickExist() {
        // given
        int[] picks = {1};
        PickFinder pickFinder = spy(new PickFinder(new PickSearcher()));
        doNothing().when(pickFinder).print();
//        when(pickSearcher.search(2)).thenReturn("this another pick");
//        PickFinder pickFinder = new PickFinder(pickSearcher);
        // when
        int result = pickFinder.pickSum(picks);
        // then
        int expected = -1;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void findPick_should_getMinusOne_when_noPicks() {
        //given
        int[] picks = {};
        PickFinder pickFinder = new PickFinder(null);
        //when
        int result = pickFinder.findPick(picks);
        //then
        int expected = -1;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void findPick_should_getPick_when_hasFirstPeak(){
        //given
        int[] picks = {2,1};
        PickFinder pickFinder = new PickFinder(null);
        //when
        int result = pickFinder.findPick(picks);
        //then
        int expected = 0;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void findPick_should_getPick_when_hasLastPeak(){
        //given
        int[] picks = {1,2};
        PickFinder pickFinder = new PickFinder(null);
        //when
        int result = pickFinder.findPick(picks);
        //then
        int expected = 1;
        Assert.assertEquals(expected, result);
    }

}
