package com.belushkin.hw11;

import com.belushkin.hw9.CollectionFamilyDao;
import com.belushkin.hw9.Family;
import com.belushkin.hw9.human.Human;
import com.belushkin.hw9.human.Man;
import com.belushkin.hw9.human.Woman;
import com.belushkin.hw9.pet.Fish;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {

    @Test
    void dsiplay_all_families_should_display_all_families() {
        //given
        Family family1 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );
        Family family2 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human()
        );
        Family family3 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human(), new Human()
        );

        //when
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);
        familyService.saveFamily(family3);

        //then
        familyService.displayAllFamilies();
    }

    @Test
    void get_families_bigger_then_works_properly() {
        //given
        Family family1 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );
        Family family2 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human()
        );
        Family family3 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human(), new Human()
        );

        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);
        familyService.saveFamily(family3);

        //when
        List<Family> biggerFamilies1 = familyService.getFamiliesBiggerThan(3);
        List<Family> biggerFamilies2 = familyService.getFamiliesBiggerThan(4);

        //then
        assertEquals(2, biggerFamilies1.size());
        assertEquals(1, biggerFamilies2.size());
    }

    @Test
    void get_families_bigger_then_return_empty_list_when_no_families() {
        //given
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        List<Family> biggerFamilies = familyService.getFamiliesBiggerThan(3);
        //then
        assertEquals(0, biggerFamilies.size());
    }

    @Test
    void get_families_less_then_works_properly() {
        //given
        Family family1 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );
        Family family2 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human()
        );
        Family family3 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human(), new Human()
        );

        com.belushkin.hw9.FamilyService familyService = new com.belushkin.hw9.FamilyService(new CollectionFamilyDao());
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);
        familyService.saveFamily(family3);
        assertEquals(3, familyService.getAllFamilies().size());
        //when
        List<Family> lessFamilies1 = familyService.getFamiliesLessThan(5);
        List<Family> lessFamilies2 = familyService.getFamiliesLessThan(3);

        //then
        assertEquals(2, lessFamilies1.size());
        assertEquals(0, lessFamilies2.size());
    }

    @Test
    void count_families_with_member_number_works_correct() {
        //given
        Family family1 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );
        Family family2 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human()
        );
        Family family3 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human()
        );

        com.belushkin.hw9.FamilyService familyService = new com.belushkin.hw9.FamilyService(new CollectionFamilyDao());
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);
        familyService.saveFamily(family3);
        assertEquals(2, familyService.getAllFamilies().size());
        //when
        //then
        assertEquals(1, familyService.countFamiliesWithMemberNumber(4));
    }

    @Test
    void delete_all_children_older_then_should_remove_children_older_then_age() {
        //given
        com.belushkin.hw9.FamilyService familyService = new com.belushkin.hw9.FamilyService(new CollectionFamilyDao());
        //when
        Family family1 = familyService.createNewFamily(
                new Man("Kis", "Mis", 2),
                new Woman("Pis", "Sis", 3)
        );

        Family family2 = familyService.createNewFamily(
                new Man("Kit", "Mit", 2),
                new Woman("Pit", "Sit", 3)
        );
        familyService.adoptChild(
                family1,
                new Human("Pet", "Fed", 12),
                new Human("Pet", "Fed", 13),
                new Human("Pet", "Fed", 14),
                new Human("Pet", "Fed", 15)
        );
        familyService.adoptChild(
                family2,
                new Human("Pet", "Fed", 1),
                new Human("Pet", "Fed", 3),
                new Human("Pet", "Fed", 4),
                new Human("Pet", "Fed", 16)
        );
        //then
        familyService.deleteAllChildrenOlderThen(10);
        assertEquals(0, familyService.getFamilyByIndex(0).getChildren().size());
        assertEquals(3, familyService.getFamilyByIndex(1).getChildren().size());
    }

}