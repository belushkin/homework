package com.belushkin.hw10;

import com.belushkin.hw8.DayOfWeek;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void constructor_should_create_correct_date_when_receives_right_date_pattern() throws ParseException {
        //given
        Human human = new Human("Man", "Portman", "31/12/1998");
        //when
        //then
        assertEquals(915055200000L, human.getBirthDate());
    }

    @Test
    void constructor_should_throw_parse_exception_when_date_pattern_is_wrong() {
        //given
        Exception exception = assertThrows(ParseException.class, () -> {
            Human human = new Human("Man", "Portman", "1998");
        });
        //when
        String expectedMessage = "Unparseable date: \"1998\"";
        String actualMessage = exception.getMessage();

        //then
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void get_years_method_return_correct_number_of_years_when_executed() throws ParseException {
        //given
        Human human = new Human("Man", "Portman", "31/12/1998");
        //when
        //then
        assertEquals("time of life: years = 21, months = 1, days = 24", human.describeAge());
    }

    @Test
    void to_string_should_return_correct_birthday_date() throws ParseException {
        //given
        Map<String, String> schedule = new HashMap< String,String>() {{
            put(DayOfWeek.Monday.name(), "walk in a park");
        }};
        Human human = new Human("Man", "Portman", "30/12/1998", 12, schedule);
        //when
        //then
        assertEquals(
                "Human{name='Man', surname='Portman', birthdate='30/12/1998', iq='12', schedule='{Monday=walk in a park}}",
                human.toString()
        );
    }

}