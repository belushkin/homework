package com.belushkin.lesson7;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyContainerTest {

    @Test
    void iterate_my_container() {
        //given
        MyContainer<Integer> integers = new MyContainer<>(1,2,3,4,5,6,7,8,9,0);
        //when
        for (Integer integer : integers) {
            System.out.println(integer);
        }
        //then
    }
}