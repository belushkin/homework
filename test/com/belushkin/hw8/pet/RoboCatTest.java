package com.belushkin.hw8.pet;

import com.belushkin.hw8.Species;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RoboCatTest {

    @Test
    void get_species_with_nickname_constructor_should_return_correct_type() {
        //given
        RoboCat roboCat = new RoboCat("Jack");
        //when
        //then
        assertEquals(Species.RoboCat, roboCat.getSpecies());
    }

    @Test
    void get_species_with_empty_constructor_should_return_correct_type() {
        //given
        RoboCat roboCat = new RoboCat();
        //when
        //then
        assertEquals(Species.RoboCat, roboCat.getSpecies());
    }

    @Test
    void get_species_with_full_constructor_should_return_correct_type() {
        //given
        Set<String> habits = new HashSet<>(Arrays.asList("eat", "sleep", "walk"));
        RoboCat roboCat = new RoboCat("Jack", 3,15, habits);
        //when
        //then
        assertEquals(Species.RoboCat, roboCat.getSpecies());
    }

}