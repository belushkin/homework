package com.belushkin.hw8.pet;

import com.belushkin.hw8.human.Man;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    @Test
    void to_string_should_be_with_species_properties_when_constructor_is_empty() {
        //given
        Dog pet = new Dog();
        //when
        //then
        assertEquals("Dog{properties=['can fly'=false,'has fur'=true,'number of legs'=4]}", pet.toString());
    }

    @Test
    void to_string_should_be_with_unknown_species_properties_when_constructor_is_empty() {
        //given
        Mouse pet = new Mouse();
        //when
        //then
        assertEquals("Unknown{properties=['can fly'=false,'has fur'=false,'number of legs'=0]}", pet.toString());
    }

    @Test
    void to_string_should_have_species_and_nickname_when_constructor_has_the_same_values() {
        //given
        Dog pet = new Dog("Atlanta");
        //when
        //then
        assertEquals("Dog{nickname='Atlanta', " +
                "properties=['can fly'=false,'has fur'=true,'number of legs'=4]" +
                "}", pet.toString());
    }

    @Test
    void to_string_should_have_all_properties_when_constructor_has_the_same_values() {
        //given
        Dog pet = new Dog(
                "Atlanta",
                12,
                10,
                new HashSet<>(Arrays.asList("eat", "sleep", "walk"))
        );
        //when
        //then
        assertEquals("Dog{" +
                "nickname='Atlanta', age='12', trickLevel='10', " +
                "properties=['can fly'=false,'has fur'=true,'number of legs'=4], " +
                "habits=[sleep, eat, walk]" +
                "}", pet.toString());
    }

    @Test
    void pet_equals_not_the_same_object_is_false() {
        //given
        Dog pet = new Dog();
        //when
        //then
        assertNotEquals(pet, new Man());
    }

    @Test
    void pet_equals_null_is_false() {
        //given
        Dog pet = new Dog();
        //when
        //then
        assertNotEquals(pet, null);
    }

    @Test
    void pet_equals_with_itself() {
        //given
        Dog pet = new Dog();
        //when
        //then
        assertEquals(pet, pet);
        assertEquals(pet.hashCode(), pet.hashCode());
    }

    @Test
    void pet_empty_objects_are_equal() {
        //given
        Dog petA = new Dog();
        Dog petB = new Dog();

        //when
        //then
        assertEquals(petA, petB);
        assertEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_equal_when_have_species_and_nickname() {
        //given
        Dog petA = new Dog("tom");
        Dog petB = new Dog("tom");

        //when
        //then
        assertEquals(petA, petB);
        assertEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_equal_with_full_parameters() {
        //given
        Set<String> habits = new HashSet<>(Arrays.asList("eat", "sleep", "walk"));
        Dog petA = new Dog("tom", 12, 90, habits);
        Dog petB = new Dog("tom", 12, 90, habits);

        //when
        //then
        assertEquals(petA, petB);
        assertEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_not_equal_with_full_parameters_and_not_equal_habits() {
        //given
        Set<String> habits1 = new HashSet<>(Arrays.asList("eat", "sleep", "walk"));
        Dog petA = new Dog( "tom", 12, 90, habits1);

        Set<String> habits2 = new HashSet<>(Arrays.asList("eat", "sleep", "run"));
        Dog petB = new Dog("tom", 12, 90, habits2);

        //when
        //then
        assertNotEquals(petA, petB);
        assertNotEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_not_equal_with_different_species() {
        //given
        Dog petA = new Dog( "tom");
        RoboCat petB = new RoboCat( "tom");

        //when
        //then
        assertNotEquals(petA, petB);
        assertNotEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_not_equal_with_different_nicknames() {
        //given
        Dog petA = new Dog( "tom");
        Dog petB = new Dog( "Jerry");

        //when
        //then
        assertNotEquals(petA, petB);
        assertNotEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_not_equal_with_different_age() {
        //given
        //given
        Set<String> habits = new HashSet<>(Arrays.asList("eat", "sleep", "walk"));
        Dog petA = new Dog("tom", 12, 90, habits);
        Dog petB = new Dog("tom", 13, 90, habits);

        //when
        //then
        assertNotEquals(petA, petB);
        assertNotEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_not_equal_with_different_trick_level() {
        //given
        //given
        Set<String> habits = new HashSet<>(Arrays.asList("eat", "sleep", "walk"));
        Dog petA = new Dog("tom", 13, 93, habits);
        Dog petB = new Dog("tom", 13, 90, habits);

        //when
        //then
        assertNotEquals(petA, petB);
        assertNotEquals(petA.hashCode(), petB.hashCode());
    }
}
