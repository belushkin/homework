package com.belushkin.hw8.pet;

import com.belushkin.hw8.Species;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MouseTest {

    @Test
    void get_species_with_nickname_constructor_should_return_unknown() {
        //given
        Mouse mouse = new Mouse("Jerry");
        //when
        //then
        assertEquals(Species.Unknown, mouse.getSpecies());
    }

    @Test
    void get_species_with_empty_constructor_should_return_unknown() {
        //given
        Mouse mouse = new Mouse();
        //when
        //then
        assertEquals(Species.Unknown, mouse.getSpecies());
    }

    @Test
    void get_species_with_full_constructor_should_return_unknown() {
        //given
        Set<String> habits = new HashSet<>(Arrays.asList("eat", "sleep", "walk"));
        Mouse mouse = new Mouse("Jack", 3,15, habits);
        //when
        //then
        assertEquals(Species.Unknown, mouse.getSpecies());
    }
}
