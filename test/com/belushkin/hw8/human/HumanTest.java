package com.belushkin.hw8.human;

import com.belushkin.hw7.pet.Fish;
import com.belushkin.hw8.DayOfWeek;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class HumanTest {

    @Test
    void to_string_should_be_empty_when_constructor_is_empty() {
        //given
        Man man = new Man();
        //when
        //then
        assertEquals("Man{}", man.toString());
    }

    @Test
    void to_string_should_have_name_and_surname_and_yaer_when_constructor_has_the_same_values() {
        //given
        Man man = new Man("Jack", "Sparrow", 2009);
        //when
        //then
        assertEquals("Man{name='Jack', surname='Sparrow', year='2009'}", man.toString());
    }

    @Test
    void to_string_should_have_all_properties_when_constructor_has_the_same_values() {
        //given
        Map<String, String> schedule = new HashMap< String,String>() {{
            put(DayOfWeek.Monday.name(), "walk in a park");
            put(DayOfWeek.Tuesday.name(), "go to school");
            put(DayOfWeek.Wednesday.name(), "watch a film");
            put(DayOfWeek.Thursday.name(), "have a rest");
            put(DayOfWeek.Friday.name(), "visit friends");
        }};

        Woman human = new Woman(
                "name",
                "surname",
                1987,
                99,
                schedule);
        //when
        //then
        assertEquals("Woman{" +
                "name='name', " +
                "surname='surname', " +
                "year='1987', " +
                "iq='99', " +
                "schedule='{Monday=walk in a park, Thursday=have a rest, Friday=visit friends, Wednesday=watch a film, Tuesday=go to school}" +
                "}", human.toString());
    }

    @Test
    void human_not_equals_with_different_object() {
        //given
        Woman human = new Woman();
        //when
        //then
        assertNotEquals(human, new Fish());
    }

    @Test
    void human_not_equals_with_null() {
        //given
        Man human = new Man();
        //when
        //then
        assertNotEquals(human, null);
    }

    @Test
    void human_equals_with_itself() {
        //given
        Man human = new Man();
        //when
        //then
        assertEquals(human, human);
        assertEquals(human.hashCode(), human.hashCode());
    }

    @Test
    void human_empty_objects_are_equal() {
        //given
        Woman humanA = new Woman();
        Woman humanB = new Woman();

        //when
        //then
        assertEquals(humanA, humanB);
        assertEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_equal_when_have_name_and_surname_and_year() {
        //given
        Man humanA = new Man("A", "B", 1);
        Man humanB = new Man("A", "B", 1);

        //when
        //then
        assertEquals(humanA, humanB);
        assertEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_not_equal_when_have_different_name() {
        //given
        Man humanA = new Man("B", "B", 1);
        Man humanB = new Man("A", "B", 1);

        //when
        //then
        assertNotEquals(humanA, humanB);
        assertNotEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_not_equal_when_have_different_surname() {
        //given
        Man humanA = new Man("A", "C", 1);
        Man humanB = new Man("A", "B", 1);

        //when
        //then
        assertNotEquals(humanA, humanB);
        assertNotEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_not_equal_when_have_different_year() {
        //given
        Man humanA = new Man("A", "B", 2);
        Man humanB = new Man("A", "B", 1);

        //when
        //then
        assertNotEquals(humanA, humanB);
        assertNotEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_equal_when_have_name_and_surname_and_year_and_iq_and_schedule() {
        //given
        //given
        Map<String, String> schedule = new HashMap< String,String>() {{
            put(DayOfWeek.Monday.name(), "walk in a park");
            put(DayOfWeek.Tuesday.name(), "go to school");
            put(DayOfWeek.Wednesday.name(), "watch a film");
            put(DayOfWeek.Thursday.name(), "have a rest");
            put(DayOfWeek.Friday.name(), "visit friends");
        }};
        Woman humanA = new Woman("A", "B", 1, 80, schedule);
        Woman humanB = new Woman("A", "B", 1, 80, schedule);

        //when
        //then
        assertEquals(humanA, humanB);
        assertEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_not_equal_when_have_different_schedule() {
        //given
        //given
        Map<String, String> schedule1 = new HashMap< String,String>() {{
            put(DayOfWeek.Monday.name(), "walk in a park");
        }};
        Map<String, String> schedule2 = new HashMap< String,String>() {{
            put(DayOfWeek.Monday.name(), "walk in the street");
        }};
        Man humanA = new Man("A", "B", 1, 80, schedule1);
        Man humanB = new Man("A", "B", 1, 80, schedule2);

        //when
        //then
        assertNotEquals(humanA, humanB);
        assertNotEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_not_equal_when_have_different_iq() {
        //given
        //given
        Map<String, String> schedule = new HashMap< String,String>() {{
            put(DayOfWeek.Monday.name(), "walk in a park");
        }};
        Man humanA = new Man("A", "B", 1, 80, schedule);
        Man humanB = new Man("A", "B", 1, 20, schedule);

        //when
        //then
        assertNotEquals(humanA, humanB);
        assertNotEquals(humanA.hashCode(), humanB.hashCode());
    }
}