package com.belushkin.hw8;

import com.belushkin.hw8.human.Human;
import com.belushkin.hw8.human.Man;
import com.belushkin.hw8.human.Woman;
import com.belushkin.hw8.pet.Dog;
import com.belushkin.hw8.pet.DomesticCat;
import com.belushkin.hw8.pet.Fish;
import com.belushkin.hw8.pet.RoboCat;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    @Test
    void to_string_should_have_mother_father_and_pet__and_no_kids_when_constructor_has_the_same_values() {
        //given
        Map<String, String> schedule = new HashMap< String,String>() {{
            put(DayOfWeek.Monday.name(), "walk in a park");
            put(DayOfWeek.Tuesday.name(), "go to school");
            put(DayOfWeek.Wednesday.name(), "watch a film");
            put(DayOfWeek.Thursday.name(), "have a rest");
            put(DayOfWeek.Friday.name(), "visit friends");
        }};
        Man father = new Man(
                "dad",
                "best",
                1987,
                99,
                schedule);

        Woman mother = new Woman(
                "mom",
                "super",
                1988);

        Set<String> habits = new HashSet<>(Arrays.asList("eat", "sleep", "walk"));
        Dog pet = new Dog(
                "Atlanta",
                12,
                10,
                habits
        );

        Family family = new Family(father, mother, pet);

        //when
        //then
        assertEquals("Family{father='Man{" +
                "name='dad', surname='best', year='1987', iq='99', " +
                "schedule='{Monday=walk in a park, Thursday=have a rest, Friday=visit friends, Wednesday=watch a film, Tuesday=go to school}" +
                "}', " +
                "mother='Woman{name='mom', surname='super', year='1988'}', " +
                "pet='Dog{nickname='Atlanta', age='12', trickLevel='10', " +
                "properties=['can fly'=false,'has fur'=true,'number of legs'=4], " +
                "habits=[sleep, eat, walk]}" +
                "}", family.toString());
    }

    @Test
    void to_string_should_have_kids_when_constructor_has_the_same_values() {
        //given
        Man father = new Man();
        Woman mother = new Woman();
        Dog pet = new Dog("Atlanta");
        Human child = new Human("kid", "daughter", 1998);

        Family family = new Family(father, mother, pet, child);
        //when
        //then
        assertEquals("Family{" +
                "father='Man{}', " +
                "mother='Woman{}', " +
                "children='[" +
                "Human{name='kid', surname='daughter', year='1998'}" +
                "]', " +
                "pet='Dog{nickname='Atlanta', " +
                "properties=['can fly'=false,'has fur'=true,'number of legs'=4]}" +
                "}", family.toString());

        assertEquals(family.getPet(), pet);
    }

    @Test
    void family_deletes_child_if_it_receives_object_which_exists_in_the_family () {
        //given
        Human child1 = new Human("kid", "daughter", 1998);
        Human child2 = new Human("kod", "son", 1997);
        Human child3 = new Human("kgd", "pap", 1998);

        Family family = new Family(
                new Man(),
                new Woman(),
                new Dog(),
                child1, child2, child3);
        //when
        assertEquals(5, family.countFamily());
        family.deleteChild(child2);

        //then
        assertEquals(4, family.countFamily());
    }

    @Test
    void family_deletes_child_if_it_receives_object_which_not_exists_in_the_family () {
        //given
        Human child1 = new Human("kid", "daughter", 1998);
        Human child2 = new Human("kod", "son", 1997);
        Human child3 = new Human("kgd", "pap", 1998);

        Family family = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                child1, child2, child3);
        //when
        assertEquals(5, family.countFamily());
        family.deleteChild(new Human("kgd", "pap", 1998));

        //then
        assertEquals(4, family.countFamily());
    }

    @Test
    void family_deletes_child_by_index_if_it_has_children () {
        //given
        Human child1 = new Human("kid", "daughter", 1998);
        Human child2 = new Human("kod", "son", 1997);
        Human child3 = new Human("kgd", "pap", 1998);

        Family family = new Family(
                new Man(),
                new Woman(),
                new RoboCat(),
                child1, child2, child3);
        //when
        assertEquals(5, family.countFamily());
        assertTrue(family.deleteChild(1));
        assertTrue(family.deleteChild(1));

        //then
        assertEquals(3, family.countFamily());
        assertEquals("Family{" +
                "father='Man{}', " +
                "mother='Woman{}', " +
                "children='[Human{name='kgd', surname='pap', year='1998'}]', " +
                "pet='RoboCat{properties=['can fly'=false,'has fur'=false,'number of legs'=4]}" +
                "}", family.toString());
    }

    @Test
    void family_does_not_deletes_child_by_index_if_it_has_not_children () {
        //given
        Family family = new Family(
                new Man(),
                new Woman(),
                new DomesticCat()
        );
        //when
        assertEquals(2, family.countFamily());
        assertFalse(family.deleteChild(1));
        assertFalse(family.deleteChild(1));

        //then
        assertEquals(2, family.countFamily());
        assertEquals("Family{" +
                "father='Man{}', " +
                "mother='Woman{}', " +
                "pet='DomesticCat{properties=['can fly'=false,'has fur'=true,'number of legs'=4]}" +
                "}", family.toString());
    }

    @Test
    void family_adds_child_correctly () {
        //given
        Family family = new Family(
                new Man(),
                new Woman(),
                new DomesticCat()
        );
        //when
        assertEquals(2, family.countFamily());
        family.addChild(new Human("kid", "daughter", 1998));

        //then
        assertEquals(3, family.countFamily());
        assertEquals("Family{" +
                "father='Man{}', " +
                "mother='Woman{}', " +
                "children='[Human{name='kid', surname='daughter', year='1998'}]', " +
                "pet='DomesticCat{properties=['can fly'=false,'has fur'=true,'number of legs'=4]}" +
                "}", family.toString());
    }

    @Test
    void family_equals_not_the_same_object_is_false() {
        //given
        Family family = new Family(new Man(), new Woman(), new DomesticCat());
        //when
        //then
        assertNotEquals(family, new Human());
    }

    @Test
    void family_equals_null_is_false() {
        //given
        Family family = new Family(new Man(), new Woman(), new Dog());
        //when
        //then
        assertNotEquals(family, null);
    }

    @Test
    void family_equals_with_itself() {
        //given
        Family family = new Family(new Man(), new Woman(), new Fish());
        //when
        //then
        assertEquals(family, family);
        assertEquals(family.hashCode(), family.hashCode());
    }

    @Test
    void family_empty_objects_are_equal() {
        //given
        Family familyA = new Family(new Man(), new Woman(), new Dog());
        Family familyB = new Family(new Man(), new Woman(), new Dog());

        //when
        //then
        assertEquals(familyA, familyB);
        assertEquals(familyA.hashCode(), familyB.hashCode());
    }

    @Test
    void family_objects_are_not_equal_with_different_father() {
        //given
        Family familyA = new Family(new Man("A", "B", 1), new Woman(), new Fish());
        Family familyB = new Family(new Man(), new Woman(), new Fish());

        //when
        //then
        assertNotEquals(familyA, familyB);
        assertNotEquals(familyA.hashCode(), familyB.hashCode());
    }

    @Test
    void family_objects_are_not_equal_with_different_mother() {
        //given
        Family familyA = new Family(new Man(), new Woman("A", "B", 1), new Fish());
        Family familyB = new Family(new Man(), new Woman(), new Fish());

        //when
        //then
        assertNotEquals(familyA, familyB);
        assertNotEquals(familyA.hashCode(), familyB.hashCode());
    }

    @Test
    void family_objects_are_not_equal_with_different_pet() {
        //given
        Family familyA = new Family(new Man(), new Woman(), new Fish("A"));
        Family familyB = new Family(new Man(), new Woman(), new Dog());

        //when
        //then
        assertNotEquals(familyA, familyB);
        assertNotEquals(familyA.hashCode(), familyB.hashCode());
    }

    @Test
    void family_objects_are_equal_with_the_same_amount_of_kids() {
        //given
        Family familyA = new Family(new Man(), new Woman(), new Fish(), new Human(), new Human());
        Family familyB = new Family(new Man(), new Woman(), new Fish(), new Human(), new Human());

        //when
        //then
        assertEquals(familyA, familyB);
        assertEquals(familyA.hashCode(), familyB.hashCode());
    }

    @Test
    void family_objects_are_not_equal_with_the_different_amount_of_kids() {
        //given
        Family familyA = new Family(new Man(), new Woman(), new Dog(), new Human());
        Family familyB = new Family(new Man(), new Woman(), new Dog(), new Human(), new Human());

        //when
        //then
        assertNotEquals(familyA, familyB);
        assertNotEquals(familyA.hashCode(), familyB.hashCode());
    }

    @Test
    void family_objects_are_not_equal_with_the_different_kids() {
        //given
        Family familyA = new Family(new Man(), new Woman(), new Dog(), new Human("A", "B", 1));
        Family familyB = new Family(new Man(), new Woman(), new Dog(), new Human());

        //when
        //then
        assertNotEquals(familyA, familyB);
        assertNotEquals(familyA.hashCode(), familyB.hashCode());
    }
}