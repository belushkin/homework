package com.belushkin.hw12;

import com.belushkin.hw12.human.Woman;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WomanTest {

    @Test
    void pretty_format_should_return_nice_string_when_called_for_woman() {

        Woman human = new Woman("Man", "Portman", "");
        assertEquals(
                "woman: {name='Man', surname='Portman', birthdate='04/03/2020'}",
                human.prettyFormat()
        );
    }
}
