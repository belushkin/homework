package com.belushkin.hw12.pet;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoboCatTest {

    @Test
    void pretty_format_should_return_nice_string_when_called_for_robo_cat() {

        RoboCat pet = new RoboCat();
        assertEquals(
                "{species='ROBOCAT'}",
                pet.prettyFormat()
        );
    }
}