package com.belushkin.hw12.pet;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MouseTest {

    @Test
    void pretty_format_should_return_nice_string_when_called_for_mouse() {

        Mouse pet = new Mouse();
        assertEquals(
                "{species='UNKNOWN'}",
                pet.prettyFormat()
        );
    }
}