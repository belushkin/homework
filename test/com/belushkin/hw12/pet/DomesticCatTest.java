package com.belushkin.hw12.pet;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DomesticCatTest {

    @Test
    void pretty_format_should_return_nice_string_when_called_for_domestic_cat() {

        DomesticCat pet = new DomesticCat();
        assertEquals(
                "{species='DOMESTICCAT'}",
                pet.prettyFormat()
        );
    }
}