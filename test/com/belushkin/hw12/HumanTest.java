package com.belushkin.hw12;

import com.belushkin.hw12.human.Human;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HumanTest {

    @Test
    void constructor_should_create_correct_date_when_receives_right_date_pattern() {
        //given
        Human human = new Human("Man", "Portman", "31/12/1998");
        //when
        //then
        assertEquals(915055200000L, human.getBirthDate());
    }

    @Test
    void constructor_should_create_current_date_when_wrong_birth_date_string_provided() {
        //given
        Human human = new Human("Man", "Portman", "1998");
        //when
        //then
        assertEquals(LocalDate.
                        now().
                        format(DateTimeFormatter.ofPattern(Human.DATE_PATTERN)),
                human.getBirthDateFormatted()
        );
    }

    @Test
    void get_years_method_return_correct_number_of_years_when_executed() {
        //given
        Human human = new Human("Man", "Portman", "");
        //when
        //then
        assertEquals("time of life: years = 0, months = 0, days = 0", human.describeAge());
    }

    @Test
    void to_string_should_return_correct_birthday_date() {
        //given
        Map<String, String> schedule = new HashMap<>() {{
            put(DayOfWeek.Monday.name(), "walk in a park");
        }};
        Human human = new Human("Man", "Portman", "30/12/1998", 12, schedule);
        //when
        //then
        assertEquals(
                "Human{name='Man', surname='Portman', birthdate='30/12/1998', iq='12', schedule='{Monday=walk in a park}'}",
                human.toString()
        );
    }

    @Test
    void pretty_format_should_return_nice_string_when_called_for_human() {

        Human human = new Human("Man", "Portman", "");
        assertEquals(
                "human: {name='Man', surname='Portman', birthdate='04/03/2020'}",
                human.prettyFormat()
        );
    }
}
