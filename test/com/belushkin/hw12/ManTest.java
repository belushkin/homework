package com.belushkin.hw12;

import com.belushkin.hw12.human.Man;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ManTest {

    @Test
    void pretty_format_should_return_nice_string_when_called_for_man() {

        Man human = new Man("Man", "Portman", "");
        assertEquals(
                "man: {name='Man', surname='Portman', birthdate='04/03/2020'}",
                human.prettyFormat()
        );
    }
}
