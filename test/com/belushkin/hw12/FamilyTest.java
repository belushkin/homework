package com.belushkin.hw12;

import com.belushkin.hw12.human.Human;
import com.belushkin.hw12.human.Man;
import com.belushkin.hw12.human.Woman;
import com.belushkin.hw12.pet.Fish;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FamilyTest {

    @Test
    void dsiplay_all_families_should_display_all_families() {
        //given
        Set<String> habits = new HashSet<>(Arrays.asList("eat", "sleep", "walk"));
        Family family = new Family(
                new Man("Kit", "Mit", "01/01/1984"),
                new Woman("Pit", "Sit", "01/01/1984"),
                new Fish("na", 44, 66, habits),
                new Human("na", "da", "01/01/2002"),
                new Human("ja", "ga", "01/01/2003")
        );
        //when
        Map<String, String> schedule = new HashMap<>() {{
            put(DayOfWeek.Monday.name(), "walk in a park");
        }};
        family.addChild(
                new Human("ra", "ba", "01/01/20013", 22, schedule)
        );
        family.getMother().bornChild();
        //then

        System.out.println(family.prettyFormat());
    }

}
