package com.belushkin.lesson8;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class DuplicateCheckerTest {

    @Test
    public void count_duplicates_return_correct_value() {
        DuplicateChecker duplicateChecker = new DuplicateChecker();

        List<Integer> collect = Stream.iterate(0, x -> x+1).
                limit(1000000).
                collect(Collectors.toList());

        assertEquals(0, duplicateChecker.getCount(collect));
        assertEquals(3, duplicateChecker.getCount(Arrays.asList(1,2,3,4,5,5,6,5,3)));
    }

}