package com.belushkin.hw6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    @Test
    void to_string_should_be_empty_when_constructor_is_empty() {
        //given
        Pet pet = new Pet();
        //when
        //then
        assertSame("{}", pet.toString());
    }

    @Test
    void to_string_should_have_species_and_nickname_when_constructor_has_the_same_values() {
        //given
        Pet pet = new Pet(Species.Dog, "Atlanta");
        //when
        //then
        assertEquals("Dog{nickname='Atlanta', " +
                "properties=['can fly'=false,'has fur'=true,'number of legs'=4]" +
                "}", pet.toString());
    }

    @Test
    void to_string_should_have_all_properties_when_constructor_has_the_same_values() {
        //given
        String[] habits = {"eat", "sleep", "walk"};
        Pet pet = new Pet(
                Species.Owl,
                "Atlanta",
                12,
                10,
                habits
        );
        //when
        //then
        assertEquals("Owl{" +
                "nickname='Atlanta', age='12', trickLevel='10', " +
                "properties=['can fly'=true,'has fur'=false,'number of legs'=2], " +
                "habits=[eat, sleep, walk]" +
                "}", pet.toString());
    }

    @Test
    void pet_equals_not_the_same_object_is_false() {
        //given
        Pet pet = new Pet();
        //when
        //then
        assertNotEquals(pet, new Human());
    }

    @Test
    void pet_equals_null_is_false() {
        //given
        Pet pet = new Pet();
        //when
        //then
        assertNotEquals(pet, null);
    }

    @Test
    void pet_equals_with_itself() {
        //given
        Pet pet = new Pet();
        //when
        //then
        assertEquals(pet, pet);
        assertEquals(pet.hashCode(), pet.hashCode());
    }

    @Test
    void pet_empty_objects_are_equal() {
        //given
        Pet petA = new Pet();
        Pet petB = new Pet();

        //when
        //then
        assertEquals(petA, petB);
        assertEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_equal_when_have_species_and_nickname() {
        //given
        Pet petA = new Pet(Species.Mouse, "tom");
        Pet petB = new Pet(Species.Mouse, "tom");

        //when
        //then
        assertEquals(petA, petB);
        assertEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_equal_with_full_parameters() {
        //given
        String[] habits = {"eat", "sleep", "walk"};
        Pet petA = new Pet(Species.Mouse, "tom", 12, 90, habits);
        Pet petB = new Pet(Species.Mouse, "tom", 12, 90, habits);

        //when
        //then
        assertEquals(petA, petB);
        assertEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_not_equal_with_full_parameters_and_not_equal_habits() {
        //given
        String[] habits1 = {"eat", "sleep", "walk"};
        Pet petA = new Pet(Species.Mouse, "tom", 12, 90, habits1);

        String[] habits2 = {"eat", "sleep", "run"};
        Pet petB = new Pet(Species.Mouse, "tom", 12, 90, habits2);

        //when
        //then
        assertNotEquals(petA, petB);
        assertNotEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_not_equal_with_different_species() {
        //given
        Pet petA = new Pet(Species.Mouse, "tom");
        Pet petB = new Pet(Species.Cat, "tom");

        //when
        //then
        assertNotEquals(petA, petB);
        assertNotEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_not_equal_with_different_nicknames() {
        //given
        Pet petA = new Pet(Species.Mouse, "tom");
        Pet petB = new Pet(Species.Mouse, "Jerry");

        //when
        //then
        assertNotEquals(petA, petB);
        assertNotEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_not_equal_with_different_age() {
        //given
        //given
        String[] habits = {"eat", "sleep", "walk"};
        Pet petA = new Pet(Species.Mouse, "tom", 12, 90, habits);
        Pet petB = new Pet(Species.Mouse, "tom", 13, 90, habits);

        //when
        //then
        assertNotEquals(petA, petB);
        assertNotEquals(petA.hashCode(), petB.hashCode());
    }

    @Test
    void pet_objects_are_not_equal_with_different_trick_level() {
        //given
        //given
        String[] habits = {"eat", "sleep", "walk"};
        Pet petA = new Pet(Species.Mouse, "tom", 13, 93, habits);
        Pet petB = new Pet(Species.Mouse, "tom", 13, 90, habits);

        //when
        //then
        assertNotEquals(petA, petB);
        assertNotEquals(petA.hashCode(), petB.hashCode());
    }
}
