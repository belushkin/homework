package com.belushkin.hw6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void to_string_should_be_empty_when_constructor_is_empty() {
        //given
        Human human = new Human();
        //when
        //then
        assertEquals("Human{}", human.toString());
    }

    @Test
    void to_string_should_have_name_and_surname_and_yaer_when_constructor_has_the_same_values() {
        //given
        Human human = new Human("Jack", "Sparrow", 2009);
        //when
        //then
        assertEquals("Human{name='Jack', surname='Sparrow', year='2009'}", human.toString());
    }

    @Test
    void to_string_should_have_all_properties_when_constructor_has_the_same_values() {
        //given
        String[][] schedule = {
                {DayOfWeek.Monday.name(), "walk in a park"},
                {DayOfWeek.Tuesday.name(), "go to school"},
                {DayOfWeek.Wednesday.name(), "watch a film"},
                {DayOfWeek.Thursday.name(), "have a rest"},
                {DayOfWeek.Friday.name(), "visit friends"},
        };

        Human human = new Human(
                "name",
                "surname",
                1987,
                99,
                schedule);
        //when
        //then
        assertEquals("Human{" +
                "name='name', " +
                "surname='surname', " +
                "year='1987', " +
                "iq='99', " +
                "schedule='[[Monday, walk in a park], [Tuesday, go to school], [Wednesday, watch a film], [Thursday, have a rest], [Friday, visit friends]]" +
                "}", human.toString());
    }

    @Test
    void human_not_equals_with_different_object() {
        //given
        Human human = new Human();
        //when
        //then
        assertNotEquals(human, new Pet());
    }

    @Test
    void human_not_equals_with_null() {
        //given
        Human human = new Human();
        //when
        //then
        assertNotEquals(human, null);
    }

    @Test
    void human_equals_with_itself() {
        //given
        Human human = new Human();
        //when
        //then
        assertEquals(human, human);
        assertEquals(human.hashCode(), human.hashCode());
    }

    @Test
    void human_empty_objects_are_equal() {
        //given
        Human humanA = new Human();
        Human humanB = new Human();

        //when
        //then
        assertEquals(humanA, humanB);
        assertEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_equal_when_have_name_and_surname_and_year() {
        //given
        Human humanA = new Human("A", "B", 1);
        Human humanB = new Human("A", "B", 1);

        //when
        //then
        assertEquals(humanA, humanB);
        assertEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_not_equal_when_have_different_name() {
        //given
        Human humanA = new Human("B", "B", 1);
        Human humanB = new Human("A", "B", 1);

        //when
        //then
        assertNotEquals(humanA, humanB);
        assertNotEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_not_equal_when_have_different_surname() {
        //given
        Human humanA = new Human("A", "C", 1);
        Human humanB = new Human("A", "B", 1);

        //when
        //then
        assertNotEquals(humanA, humanB);
        assertNotEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_not_equal_when_have_different_year() {
        //given
        Human humanA = new Human("A", "B", 2);
        Human humanB = new Human("A", "B", 1);

        //when
        //then
        assertNotEquals(humanA, humanB);
        assertNotEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_equal_when_have_name_and_surname_and_year_and_iq_and_schedule() {
        //given
        //given
        String[][] schedule = {
                {DayOfWeek.Monday.name(), "walk in a park"},
                {DayOfWeek.Tuesday.name(), "go to school"},
                {DayOfWeek.Wednesday.name(), "watch a film"},
                {DayOfWeek.Thursday.name(), "have a rest"},
                {DayOfWeek.Friday.name(), "visit friends"},
        };
        Human humanA = new Human("A", "B", 1, 80, schedule);
        Human humanB = new Human("A", "B", 1, 80, schedule);

        //when
        //then
        assertEquals(humanA, humanB);
        assertEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_not_equal_when_have_different_schedule() {
        //given
        //given
        String[][] schedule1 = {
                {DayOfWeek.Monday.name(), "walk in a park"}
        };
        String[][] schedule2 = {
                {DayOfWeek.Monday.name(), "walk in the street"}
        };
        Human humanA = new Human("A", "B", 1, 80, schedule1);
        Human humanB = new Human("A", "B", 1, 80, schedule2);

        //when
        //then
        assertNotEquals(humanA, humanB);
        assertNotEquals(humanA.hashCode(), humanB.hashCode());
    }

    @Test
    void human_objects_are_not_equal_when_have_different_iq() {
        //given
        //given
        String[][] schedule = {
                {DayOfWeek.Monday.name(), "walk in a park"}
        };
        Human humanA = new Human("A", "B", 1, 80, schedule);
        Human humanB = new Human("A", "B", 1, 20, schedule);

        //when
        //then
        assertNotEquals(humanA, humanB);
        assertNotEquals(humanA.hashCode(), humanB.hashCode());
    }
}