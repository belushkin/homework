package com.belushkin.hw6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    @Test
    void to_string_should_have_mother_father_and_pet__and_no_kids_when_constructor_has_the_same_values() {
        //given
        String[][] schedule = {
                {DayOfWeek.Monday.name(), "walk in a park"},
                {DayOfWeek.Tuesday.name(), "go to school"},
                {DayOfWeek.Wednesday.name(), "watch a film"},
                {DayOfWeek.Thursday.name(), "have a rest"},
                {DayOfWeek.Friday.name(), "visit friends"},
        };
        Human father = new Human(
                "dad",
                "best",
                1987,
                99,
                schedule);

        Human mother = new Human(
                "mom",
                "super",
                1988);

        String[] habits = {"eat", "sleep", "walk"};
        Pet pet = new Pet(
                Species.Owl,
                "Atlanta",
                12,
                10,
                habits
        );

        Family family = new Family(father, mother, pet);

        //when
        //then
        assertEquals("Family{father='Human{" +
                "name='dad', surname='best', year='1987', iq='99', " +
                "schedule='[[Monday, walk in a park], [Tuesday, go to school], [Wednesday, watch a film], [Thursday, have a rest], [Friday, visit friends]]" +
                "}', " +
                "mother='Human{name='mom', surname='super', year='1988'}', " +
                "pet='Owl{nickname='Atlanta', age='12', trickLevel='10', " +
                "properties=['can fly'=true,'has fur'=false,'number of legs'=2], " +
                "habits=[eat, sleep, walk]}" +
                "}", family.toString());
    }

    @Test
    void to_string_should_have_kids_when_constructor_has_the_same_values() {
        //given
        Human father = new Human();
        Human mother = new Human();
        Pet pet = new Pet(Species.Dog, "Atlanta");
        Human child = new Human("kid", "daughter", 1998);

        Family family = new Family(father, mother, pet, child);
        //when
        //then
        assertEquals("Family{" +
                "father='Human{}', " +
                "mother='Human{}', " +
                "children='[" +
                "Human{name='kid', surname='daughter', year='1998'}" +
                "]', " +
                "pet='Dog{nickname='Atlanta', " +
                "properties=['can fly'=false,'has fur'=true,'number of legs'=4]}" +
                "}", family.toString());
    }

    @Test
    void family_deletes_child_if_it_receives_object_which_exists_in_the_family () {
        //given
        Human child1 = new Human("kid", "daughter", 1998);
        Human child2 = new Human("kod", "son", 1997);
        Human child3 = new Human("kgd", "pap", 1998);

        Family family = new Family(
                new Human(),
                new Human(),
                new Pet(),
                child1, child2, child3);
        //when
        assertEquals(5, family.countFamily());
        family.deleteChild(child2);

        //then
        assertEquals(4, family.countFamily());
    }

    @Test
    void family_deletes_child_if_it_receives_object_which_not_exists_in_the_family () {
        //given
        Human child1 = new Human("kid", "daughter", 1998);
        Human child2 = new Human("kod", "son", 1997);
        Human child3 = new Human("kgd", "pap", 1998);

        Family family = new Family(
                new Human(),
                new Human(),
                new Pet(),
                child1, child2, child3);
        //when
        assertEquals(5, family.countFamily());
        family.deleteChild(new Human("kgd", "pap", 1998));

        //then
        assertEquals(4, family.countFamily());
    }

    @Test
    void family_deletes_child_by_index_if_it_has_children () {
        //given
        Human child1 = new Human("kid", "daughter", 1998);
        Human child2 = new Human("kod", "son", 1997);
        Human child3 = new Human("kgd", "pap", 1998);

        Family family = new Family(
                new Human(),
                new Human(),
                new Pet(),
                child1, child2, child3);
        //when
        assertEquals(5, family.countFamily());
        assertTrue(family.deleteChild(0));
        assertTrue(family.deleteChild(0));

        //then
        assertEquals(3, family.countFamily());
        assertEquals("Family{" +
                "father='Human{}', " +
                "mother='Human{}', " +
                "children='[Human{name='kgd', surname='pap', year='1998'}]', " +
                "pet='{}" +
                "}", family.toString());
    }

    @Test
    void family_does_not_deletes_child_by_index_if_it_has_not_children () {
        //given
        Family family = new Family(
                new Human(),
                new Human(),
                new Pet()
        );
        //when
        assertEquals(2, family.countFamily());
        assertFalse(family.deleteChild(0));
        assertFalse(family.deleteChild(0));

        //then
        assertEquals(2, family.countFamily());
        assertEquals("Family{" +
                "father='Human{}', " +
                "mother='Human{}', " +
                "pet='{}" +
                "}", family.toString());
    }

    @Test
    void family_adds_child_correctly () {
        //given
        Family family = new Family(
                new Human(),
                new Human(),
                new Pet()
        );
        //when
        assertEquals(2, family.countFamily());
        family.addChild(new Human("kid", "daughter", 1998));

        //then
        assertEquals(3, family.countFamily());
        assertEquals("Family{" +
                "father='Human{}', " +
                "mother='Human{}', " +
                "children='[Human{name='kid', surname='daughter', year='1998'}]', " +
                "pet='{}" +
                "}", family.toString());
    }

    @Test
    void family_equals_not_the_same_object_is_false() {
        //given
        Family family = new Family(new Human(), new Human(), new Pet());
        //when
        //then
        assertNotEquals(family, new Human());
    }

    @Test
    void family_equals_null_is_false() {
        //given
        Family family = new Family(new Human(), new Human(), new Pet());
        //when
        //then
        assertNotEquals(family, null);
    }

    @Test
    void family_equals_with_itself() {
        //given
        Family family = new Family(new Human(), new Human(), new Pet());
        //when
        //then
        assertEquals(family, family);
        assertEquals(family.hashCode(), family.hashCode());
    }

    @Test
    void family_empty_objects_are_equal() {
        //given
        Family familyA = new Family(new Human(), new Human(), new Pet());
        Family familyB = new Family(new Human(), new Human(), new Pet());

        //when
        //then
        assertEquals(familyA, familyB);
        assertEquals(familyA.hashCode(), familyB.hashCode());
    }

    @Test
    void family_objects_are_not_equal_with_different_father() {
        //given
        Family familyA = new Family(new Human("A", "B", 1), new Human(), new Pet());
        Family familyB = new Family(new Human(), new Human(), new Pet());

        //when
        //then
        assertNotEquals(familyA, familyB);
        assertNotEquals(familyA.hashCode(), familyB.hashCode());
    }

    @Test
    void family_objects_are_not_equal_with_different_mother() {
        //given
        Family familyA = new Family(new Human(), new Human("A", "B", 1), new Pet());
        Family familyB = new Family(new Human(), new Human(), new Pet());

        //when
        //then
        assertNotEquals(familyA, familyB);
        assertNotEquals(familyA.hashCode(), familyB.hashCode());
    }

    @Test
    void family_objects_are_not_equal_with_different_pet() {
        //given
        Family familyA = new Family(new Human(), new Human(), new Pet(Species.Mouse, "A"));
        Family familyB = new Family(new Human(), new Human(), new Pet());

        //when
        //then
        assertNotEquals(familyA, familyB);
        assertNotEquals(familyA.hashCode(), familyB.hashCode());
    }

    @Test
    void family_objects_are_equal_with_the_same_amount_of_kids() {
        //given
        Family familyA = new Family(new Human(), new Human(), new Pet(), new Human(), new Human());
        Family familyB = new Family(new Human(), new Human(), new Pet(), new Human(), new Human());

        //when
        //then
        assertEquals(familyA, familyB);
        assertEquals(familyA.hashCode(), familyB.hashCode());
    }

    @Test
    void family_objects_are_not_equal_with_the_different_amount_of_kids() {
        //given
        Family familyA = new Family(new Human(), new Human(), new Pet(), new Human());
        Family familyB = new Family(new Human(), new Human(), new Pet(), new Human(), new Human());

        //when
        //then
        assertNotEquals(familyA, familyB);
        assertNotEquals(familyA.hashCode(), familyB.hashCode());
    }

    @Test
    void family_objects_are_not_equal_with_the_different_kids() {
        //given
        Family familyA = new Family(new Human(), new Human(), new Pet(), new Human("A", "B", 1));
        Family familyB = new Family(new Human(), new Human(), new Pet(), new Human());

        //when
        //then
        assertNotEquals(familyA, familyB);
        assertNotEquals(familyA.hashCode(), familyB.hashCode());
    }
}