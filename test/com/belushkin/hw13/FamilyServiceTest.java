package com.belushkin.hw13;

import com.belushkin.hw13.human.Human;
import com.belushkin.hw13.human.Man;
import com.belushkin.hw13.human.Woman;
import com.belushkin.hw13.pet.Fish;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {

    @Test
    void save_data_stores_generated_list_of_families_when_called() {
        //given
        Family family = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );
        ArrayList<Family> families = new ArrayList<>(
                Collections.singletonList(family)
        );

        //when
        FamilyService familyService = new FamilyService(new CollectionFamilyDao(), "hw13_test.data");
        familyService.saveData(families);

        //then
        familyService.loadData(familyService.prepareData());
        Man father = familyService.getFamilyByIndex(0).getFather();
        assertEquals(father, new Man());
    }

}