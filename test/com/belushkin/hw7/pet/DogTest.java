package com.belushkin.hw7.pet;

import com.belushkin.hw7.Species;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DogTest {

    @Test
    void get_species_with_nickname_constructor_should_return_correct_type() {
        //given
        Dog dog = new Dog("Jack");
        //when
        //then
        assertEquals(Species.Dog, dog.getSpecies());
    }

    @Test
    void get_species_with_empty_constructor_should_return_correct_type() {
        //given
        Dog dog = new Dog();
        //when
        //then
        assertEquals(Species.Dog, dog.getSpecies());
    }

    @Test
    void get_species_with_full_constructor_should_return_correct_type() {
        //given
        String[] habits = {"eat", "sleep", "walk"};
        Dog dog = new Dog("Jack", 3,15, habits);
        //when
        //then
        assertEquals(Species.Dog, dog.getSpecies());
    }

}
