package com.belushkin.hw7.pet;

import com.belushkin.hw7.Species;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DomesticCatTest {

    @Test
    void get_species_with_nickname_constructor_should_return_correct_type() {
        //given
        DomesticCat domesticCat = new DomesticCat("Jack");
        //when
        //then
        assertEquals(Species.DomesticCat, domesticCat.getSpecies());
    }

    @Test
    void get_species_with_empty_constructor_should_return_correct_type() {
        //given
        DomesticCat domesticCat = new DomesticCat();
        //when
        //then
        assertEquals(Species.DomesticCat, domesticCat.getSpecies());
    }

    @Test
    void get_species_with_full_constructor_should_return_correct_type() {
        //given
        String[] habits = {"eat", "sleep", "walk"};
        DomesticCat domesticCat = new DomesticCat("Jack", 3,15, habits);
        //when
        //then
        assertEquals(Species.DomesticCat, domesticCat.getSpecies());
    }

}