package com.belushkin.hw7.pet;

import com.belushkin.hw7.Species;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MouseTest {

    @Test
    void get_species_with_nickname_constructor_should_return_unknown() {
        //given
        Mouse mouse = new Mouse("Jerry");
        //when
        //then
        assertEquals(Species.Unknown, mouse.getSpecies());
    }

    @Test
    void get_species_with_empty_constructor_should_return_unknown() {
        //given
        Mouse mouse = new Mouse();
        //when
        //then
        assertEquals(Species.Unknown, mouse.getSpecies());
    }

    @Test
    void get_species_with_full_constructor_should_return_unknown() {
        //given
        String[] habits = {"eat", "sleep", "walk"};
        Mouse mouse = new Mouse("Jack", 3,15, habits);
        //when
        //then
        assertEquals(Species.Unknown, mouse.getSpecies());
    }
}
