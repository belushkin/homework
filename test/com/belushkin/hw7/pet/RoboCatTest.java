package com.belushkin.hw7.pet;

import com.belushkin.hw7.Species;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoboCatTest {

    @Test
    void get_species_with_nickname_constructor_should_return_correct_type() {
        //given
        RoboCat roboCat = new RoboCat("Jack");
        //when
        //then
        assertEquals(Species.RoboCat, roboCat.getSpecies());
    }

    @Test
    void get_species_with_empty_constructor_should_return_correct_type() {
        //given
        RoboCat roboCat = new RoboCat();
        //when
        //then
        assertEquals(Species.RoboCat, roboCat.getSpecies());
    }

    @Test
    void get_species_with_full_constructor_should_return_correct_type() {
        //given
        String[] habits = {"eat", "sleep", "walk"};
        RoboCat roboCat = new RoboCat("Jack", 3,15, habits);
        //when
        //then
        assertEquals(Species.RoboCat, roboCat.getSpecies());
    }

}