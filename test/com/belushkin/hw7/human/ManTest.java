package com.belushkin.hw7.human;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ManTest {

    @Test
    void custom_method_return_string() {
        //given
        Woman woman = new Woman();
        //when
        //then
        assertEquals("I am making up", woman.makeup());
    }

}