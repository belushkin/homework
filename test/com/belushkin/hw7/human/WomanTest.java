package com.belushkin.hw7.human;

import com.belushkin.hw7.Family;
import com.belushkin.hw7.pet.RoboCat;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WomanTest {

    @Test
    void custom_method_return_string() {
        //given
        Man man = new Man();
        //when
        //then
        assertEquals("I am repairing car", man.repairCar());
    }

    @Test
    void bornChild() {
        //given
        Man father = new Man("Max", "Pain", 56, 20, null);
        Woman mother = new Woman("Gabriela", "Rot", 55, 20, null);
        RoboCat pet = new RoboCat("tom");
        Family family = new Family(father, mother, pet);
        //when
        Human child = mother.bornChild();
        //then
        assertEquals("Pain", child.getSurname());
        assertEquals(3, family.countFamily());
        assertEquals(20, child.getIq());

    }
}