package com.belushkin.hw9;

import com.belushkin.hw9.human.Human;
import com.belushkin.hw9.human.Man;
import com.belushkin.hw9.human.Woman;
import com.belushkin.hw9.pet.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {

    @Test
    void save_family_should_add_family_to_the_list() {
        //given
        Family family = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
                );
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        assertEquals(0, familyService.getAllFamilies().size());
        //when
        familyService.saveFamily(family);
        //then
        assertEquals(1, familyService.getAllFamilies().size());
    }

    @Test
    void get_family_by_correct_index_return_family() {
        //given
        Family family = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        familyService.saveFamily(family);
        //then
        assertEquals(family, familyService.getFamilyByIndex(0));
        assertEquals(family, familyService.getFamilyById(0));
    }

    @Test
    void get_family_by_incorrect_index_return_null() {
        //given
        Family family = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        familyService.saveFamily(family);
        //then
        assertNull(familyService.getFamilyByIndex(1));
        assertNull(familyService.getFamilyById(1));
    }

    @Test
    void delete_family_by_correct_family_works_properly() {
        //given
        Family family = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        assertEquals(0, familyService.getAllFamilies().size());
        familyService.saveFamily(family);
        assertEquals(1, familyService.getAllFamilies().size());
        //then
        assertTrue(familyService.deleteFamily(family));
        assertEquals(0, familyService.getAllFamilies().size());
    }

    @Test
    void delete_family_by_incorrect_family_does_not_delete_family() {
        //given
        Family family1 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );

        Family family2 = new Family(
                new Man("Sandro", "Phan", 12),
                new Woman(),
                new Fish(),
                new Human()
        );

        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        familyService.saveFamily(family1);
        assertEquals(1, familyService.getAllFamilies().size());
        //then
        assertFalse(familyService.deleteFamily(family2));
        assertEquals(1, familyService.getAllFamilies().size());
    }

    @Test
    void delete_family_by_correct_index_works_properly() {
        //given
        Family family = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        assertEquals(0, familyService.getAllFamilies().size());
        familyService.saveFamily(family);
        assertEquals(1, familyService.getAllFamilies().size());
        //then
        assertTrue(familyService.deleteFamily(0));
        assertEquals(0, familyService.getAllFamilies().size());
    }

    @Test
    void delete_family_by_incorrect_index_does_not_delete_family() {
        //given
        Family family = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );

        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        familyService.saveFamily(family);
        assertEquals(1, familyService.getAllFamilies().size());
        //then
        assertFalse(familyService.deleteFamily(4));
        assertEquals(1, familyService.getAllFamilies().size());
    }

    @Test
    void get_families_bigger_then_works_properly() {
        //given
        Family family1 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );
        Family family2 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human()
        );
        Family family3 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human(), new Human()
        );

        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);
        familyService.saveFamily(family3);
        assertEquals(3, familyService.getAllFamilies().size());
        //when
        List<Family> biggerFamilies1 = familyService.getFamiliesBiggerThan(3);
        List<Family> biggerFamilies2 = familyService.getFamiliesBiggerThan(4);

        //then
        assertEquals(2, biggerFamilies1.size());
        assertEquals(1, biggerFamilies2.size());
    }

    @Test
    void get_families_less_then_works_properly() {
        //given
        Family family1 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );
        Family family2 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human()
        );
        Family family3 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human(), new Human()
        );

        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);
        familyService.saveFamily(family3);
        assertEquals(3, familyService.getAllFamilies().size());
        //when
        List<Family> lessFamilies1 = familyService.getFamiliesLessThan(5);
        List<Family> lessFamilies2 = familyService.getFamiliesLessThan(3);

        //then
        assertEquals(2, lessFamilies1.size());
        assertEquals(0, lessFamilies2.size());
    }

    @Test
    void count_families_with_member_number_works_correct() {
        //given
        Family family1 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human()
        );
        Family family2 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human()
        );
        Family family3 = new Family(
                new Man(),
                new Woman(),
                new Fish(),
                new Human(), new Human()
        );

        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        familyService.saveFamily(family1);
        familyService.saveFamily(family2);
        familyService.saveFamily(family3);
        assertEquals(2, familyService.getAllFamilies().size());
        //when
        //then
        assertEquals(1, familyService.countFamiliesWithMemberNumber(4));
    }

    @Test
    void create_new_family_works_correctly() {
        //given
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        familyService.createNewFamily(
                new Man(),
                new Woman()
        );
        //then
        assertEquals(1, familyService.getAllFamilies().size());
    }

    @Test
    void delete_family_by_index_works() {
        //given
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        familyService.createNewFamily(
                new Man(),
                new Woman()
        );
        familyService.createNewFamily(
                new Man(),
                new Woman()
        );
        assertEquals(1, familyService.getAllFamilies().size());
        //then
        assertFalse(familyService.deleteFamilyByIndex(3));
        assertTrue(familyService.deleteFamilyByIndex(0));
        assertEquals(0, familyService.getAllFamilies().size());
    }

    @Test
    void born_child_updates_family_in_db() {
        //given
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        familyService.createNewFamily(
                new Man("Ki", "Mi", 2),
                new Woman("Pi", "Si", 3)
        );

        assertEquals(1, familyService.getAllFamilies().size());
        //then
        Family family = familyService.getFamilyByIndex(0);
        familyService.bornChild(family, "Meshok", "Nata");
        assertEquals(1, family.getChildren().size());

        List<Human> children = family.getChildren();
        Human kid = children.get(0);

        if (kid.getClass().getSimpleName().equals("Man")) {
            assertEquals(kid.getName(), "Meshok");
        } else {
            assertEquals(kid.getName(), "Nata");
        }
    }

    @Test
    void save_family_dao_should_update_family_when_family_exists() {
        //given
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        Family family = familyService.createNewFamily(
                new Man("Ki", "Mi", 2),
                new Woman("Pi", "Si", 3)
        );
        family.addPet(new Fish());
        familyService.saveFamily(family);
        //then
        assertEquals(1, family.getPets().size());
        assertEquals(1, familyService.getAllFamilies().size());
    }

    @Test
    void adopt_child_add_child_to_the_family_when_adopt_method_called() {
        //given
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        familyService.createNewFamily(
                new Man("Ki", "Mi", 2),
                new Woman("Pi", "Si", 3)
        );

        assertEquals(1, familyService.getAllFamilies().size());
        //then
        Family family = familyService.getFamilyByIndex(0);
        Family updatedFamily = familyService.adoptChild(
                family,
                new Human("Pet", "Fed", 12)
        );
        assertEquals(1, family.getChildren().size());
        assertEquals(1, updatedFamily.getChildren().size());
    }

    @Test
    void delete_all_children_older_then_should_remove_children_older_then_age() {
        //given
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        Family family1 = familyService.createNewFamily(
                new Man("Kis", "Mis", 2),
                new Woman("Pis", "Sis", 3)
        );

        Family family2 = familyService.createNewFamily(
                new Man("Kit", "Mit", 2),
                new Woman("Pit", "Sit", 3)
        );
        familyService.adoptChild(
                family1,
                new Human("Pet", "Fed", 12),
                new Human("Pet", "Fed", 13),
                new Human("Pet", "Fed", 14),
                new Human("Pet", "Fed", 15)
        );
        familyService.adoptChild(
                family2,
                new Human("Pet", "Fed", 1),
                new Human("Pet", "Fed", 3),
                new Human("Pet", "Fed", 4),
                new Human("Pet", "Fed", 16)
        );
        //then
        familyService.deleteAllChildrenOlderThen(10);
        assertEquals(0, familyService.getFamilyByIndex(0).getChildren().size());
        assertEquals(3, familyService.getFamilyByIndex(1).getChildren().size());
    }

    @Test
    void method_count_returns_size_of_all_families() {
        //given
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        familyService.createNewFamily(
                new Man("Ki", "Mi", 2),
                new Woman("Pi", "Si", 3)
        );

        //then
        assertEquals(1, familyService.count());
    }

    @Test
    void method_get_pets_should_return_set_of_pets_when_pets_exist() {
        //given
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        Family family = familyService.createNewFamily(
                new Man("Ki", "Mi", 2),
                new Woman("Pi", "Si", 3)
        );
        family.addPet(
                new Fish(), new Dog(), new DomesticCat(), new RoboCat()
        );

        //then
        assertEquals(4, familyService.getPets(0).size());
    }

    @Test
    void method_get_pets_should_not_return_set_of_pets_when_pets_does_not_exist() {
        //given
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        Family family = familyService.createNewFamily(
                new Man("Ki", "Mi", 2),
                new Woman("Pi", "Si", 3)
        );

        //then
        assertTrue(familyService.getPets(0).isEmpty());
        assertEquals(0, familyService.getPets(0).size());
    }

    @Test
    void method_add_pet_should_add_pet_to_the_db_when_pet_exists() {
        //given
        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        //when
        familyService.createNewFamily(
                new Man("Ki", "Mi", 2),
                new Woman("Pi", "Si", 3)
        );
        familyService.addPet(
                0,
                new Fish(), new Dog(), new DomesticCat(), new RoboCat()
        );
        //then
        assertFalse(familyService.getPets(0).isEmpty());
        assertEquals(4, familyService.getPets(0).size());
    }

}
