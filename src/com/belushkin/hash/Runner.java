package com.belushkin.hash;

import java.io.*;

// https://www.browserling.com/tools/prime-numbers
public class Runner {

    private final int m = 2000083;//10
    private long[] T = new long[m];
    private long[] A = new long[1000000];

    public static void main(String[] args) {
        Runner runner = new Runner();
        runner.generateHashMap();
        runner.calcS();
//        runner.printT();
    }

    private void calcS() {
        int total = 0;
        for (int s = -1000; s <= 1000; s++) {
            int amount = 0;
            for (long l : A) {
                long y = s - l;
                if (hashSearch(y)) {
                    amount++;
//                    System.out.println("S=" + s);
//                    System.out.println("x=" + A[i]);
//                    System.out.println("y=" + y);
//                    System.out.println();
                }
            }
            if (amount > 0) {
                System.out.println(s);
                total++;
            }
        }
        System.out.println("total=" + total);

    }

    private void generateHashMap() {
        try
        {
            String filename = "/home/maxbelushkin/workspace/homework/src/com/belushkin/hash/input_06.txt";
            File file=new File(filename);
            FileReader fr=new FileReader(file);
            BufferedReader br=new BufferedReader(fr);
            String line;
            int i = 0;
            while((line = br.readLine()) != null)
            {
//                if (i > 5) break;
//                System.out.println(line);
//                System.out.println(Long.parseLong(line));
                long num = Long.parseLong(line);
                A[i] = num;
                hashInsert(num);
                i++;
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void printT() {
        for (int i = 0; i < 10; i++) {
            System.out.println(T[i]);
        }
    }

    private long hashInsert(long k) {
        int i = 0;
        while (i != m) {
            int j = h(k, i);
            if (T[j] == 0) {
                T[j] = k;
                return j;
            } else {
                i++;
            }
        }
        return -1;
    }

    private boolean hashSearch(long k) {
        int i = 0;
        while (i != m) {
            int j = h(k, i);
            if (T[j] == k) {
                return true;
            } else if (T[j] != 0) {
                i++;
            } else {
                break;
            }
        }
        return false;
    }

    private int h(long k, int i) {
        return Math.floorMod((h1(k) + h2(k)*i),m);
    }

    private int h1(long k) {
        return Math.floorMod(k,m);
    }

    private int h2(long k) {
        return 1 + Math.floorMod(k,m-1);
    }

}
