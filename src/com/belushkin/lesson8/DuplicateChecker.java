package com.belushkin.lesson8;

import java.util.Collections;
import java.util.List;

public class DuplicateChecker {

    <T extends Comparable <T>> long getCount(List<T> args) {

        Collections.sort(args);
        int counter = 0;

        for (int i = 1; i < args.size(); i++) {
            if (args.get(i-1) == args.get(i)) {
                counter++;
            }
        }

        return counter;
    }
}
