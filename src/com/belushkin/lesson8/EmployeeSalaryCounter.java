package com.belushkin.lesson8;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmployeeSalaryCounter {

    public static String getEmployeesSalary(List<Employee> employees) {
        List<String> salaries = new ArrayList<>();

        employees.stream().forEach(employee -> {
            BigDecimal toAdd = employee.getSalaries().
                    stream().
                    reduce(BigDecimal::add).
                    orElse(null);

            salaries.add(employee.getName() + " - " + toAdd);
        });
        return salaries.stream().collect(Collectors.joining(","));
    }

    public static String getEmployeesSalary2(List<Employee> employees) {
        Map<String, BigDecimal> collect = employees.
                stream().
                collect(Collectors.toMap(
                        e -> e.getName(),
                        e -> e.getSalaries().
                                stream().
                                reduce((p, a) -> p.add(a)).orElse(null)));
        return collect.
                entrySet().
                stream().
                map(e->e.getKey()+" "+e.getValue()).
                collect(Collectors.joining("-"));
    }

    public static void main(String[] args) {
        String s = getEmployeesSalary(Arrays.asList(
                new Employee("Tonya",
                        Arrays.asList(new BigDecimal(10), new BigDecimal(20))
                ),
                new Employee("Totya",
                        Arrays.asList(new BigDecimal(30), new BigDecimal(40))
                )
        ));
        System.out.println(s);
    }
}

class Employee {

    private final String name;
    List<BigDecimal> salaries;

    Employee(String name, List<BigDecimal> salaries) {

        this.name = name;
        this.salaries = salaries;
    }
    public List<BigDecimal> getSalaries() {
        return salaries;
    }

    public String getName() {
        return name;
    }
}
