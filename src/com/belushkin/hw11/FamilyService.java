package com.belushkin.hw11;

import com.belushkin.hw9.Family;
import com.belushkin.hw9.FamilyDao;
import com.belushkin.hw9.human.Human;
import com.belushkin.hw9.human.Man;
import com.belushkin.hw9.human.Woman;
import com.belushkin.hw9.pet.Pet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FamilyService {

    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public Family getFamilyByIndex(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public boolean deleteFamily(int index) {
        return familyDao.deleteFamily(index);
    }

    public boolean deleteFamily(Family family) {
        return familyDao.deleteFamily(family);
    }

    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

    public void displayAllFamilies() {
        familyDao.getAllFamilies().forEach(System.out::println);
    }

    public List<Family> getFamiliesBiggerThan(int amount) {

        return familyDao.
                getAllFamilies().
                stream().
                filter(family -> family.countFamily() > amount).
                collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int amount) {
        return familyDao.
                getAllFamilies().
                stream().
                filter(family -> family.countFamily() < amount).
                collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int amount) {
        return (int) familyDao.
                getAllFamilies().
                stream().
                filter(family -> family.countFamily() == amount).
                count();
    }

    public Family createNewFamily(Man man, Woman woman) {
        Family family = new Family(
                man,
                woman,
                null
        );
        familyDao.saveFamily(family);
        return family;
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public void bornChild(Family family, String maleName, String femaleName) {
        Woman mother = family.getMother();

        Human child = mother.bornChild();
        if (child.getClass().getSimpleName().equals("Man")) {
            child.setName(maleName);
        } else {
            child.setName(femaleName);
        }
    }

    public Family adoptChild(Family family, Human... child) {
        for (Human human : child) {
            family.addChild(human);
        }
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyDao.
                getAllFamilies().
                forEach(
                        family -> family.
                                getChildren().
                                removeIf(child -> child.getYear() > age)
                );
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int familyIndex) {
        Family family = getFamilyByIndex(familyIndex);
        if (family != null) {
            return family.getPets();
        }
        return Collections.emptySet();
    }

    public void addPet(int familyIndex, Pet... animal) {
        Family family = getFamilyByIndex(familyIndex);
        if (family != null) {
            family.addPet(animal);
        }
    }
}
