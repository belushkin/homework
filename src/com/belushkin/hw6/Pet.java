package com.belushkin.hw6;

import java.util.Arrays;
import java.util.Objects;

public class Pet {

    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    private final int TRICKNESS_LEVEL = 50;

    static {
        System.out.println(Pet.class.getName() + " is loading");
    }

    {
        System.out.println(Pet.class.getName() + " was created");
    }

    public Pet(Species species, String nickname){

        this.species = species;
        this.nickname = nickname;
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits){

        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(){

    }

    public String getNickname() {
        return nickname;
    }

    public Species getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String getTrickLevelAsString() {
        return getTrickLevel() > TRICKNESS_LEVEL ? "tricky" : "not tricky";
    }

    public void eat() {
        System.out.println("I am eating!");
    }

    public void respond() {
        if (getNickname() != null) {
            StringBuilder message = new StringBuilder("Hello host. I am ").
                    append(getNickname()).
                    append(". I am boring");
            System.out.println(message.toString());
        }
    }

    public void foul() {
        if (getNickname() != null) {
            System.out.println("Cover all tracks well is needed...");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;

        if (species == null && pet.species != null) {
            return false;
        }
        if (nickname == null && pet.nickname != null) {
            return false;
        }
        if (habits == null && pet.habits != null) {
            return false;
        }

        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                (species == null || species.equals(pet.species)) &&
                (nickname == null || nickname.equals(pet.nickname)) &&
                (habits == null || Arrays.equals(habits, pet.habits));
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname, age, trickLevel, TRICKNESS_LEVEL);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }

    @Override
    public String toString() {
        if (species == null) {
            return "{}";
        }

        StringBuilder pet = new StringBuilder(species + "{");
        if (nickname != null) {
            pet.
                    append("nickname='").
                    append(nickname).
                    append("', ");
        }

        if (age > 0) {
            pet.
                    append("age='").
                    append(age).
                    append("', ");
        }

        if (trickLevel > 0) {
            pet.
                    append("trickLevel='").
                    append(trickLevel).
                    append("', ");
        }

        pet.
                append("properties=[").
                append("'can fly'=").append(species.isCanFly()).
                append(",").
                append("'has fur'=").append(species.isHasFur()).
                append(",").
                append("'number of legs'=").append(species.getNumberOfLegs()).
                append("], ");

        if (habits != null) {
            pet.
                    append("habits=").
                    append(Arrays.toString(habits)).
                    append("}");
        } else {
            pet.
                    append("}");
        }

        if (pet.charAt(pet.length()-3) == ',') {
            pet.deleteCharAt(pet.length()-3);
            pet.deleteCharAt(pet.length()-2);
        }
        return pet.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Garbage Collector, pet: " + this);
    }

}
