package com.belushkin.hw6;

public enum Species {
    Cat(false, 4, true),
    Dog(false, 4, true),
    Mouse(false, 4, false),
    Owl(true, 2, false);

    private final boolean canFly;
    private final int numberOfLegs;
    private final boolean hasFur;


    Species(boolean canFly, int numberOfLegs, boolean hasFur) {

        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    public boolean isCanFly() {
        return canFly;
    }

    public boolean isHasFur() {
        return hasFur;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

}
