package com.belushkin.lesson4;

import java.util.Objects;

public class User {

    private int age;
    private final String name;

    public User(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        User user = (User) o;
//        return age == user.age &&
//                name.equals(user.name);
//    }

    @Override
    public int hashCode() {
        return Objects.hash(age, name);
    }

    @Override
    public String toString() {
//        return "this is user name = " + name + " and age is " + age;
        return new StringBuilder("this is user name = ")
                .append(name)
                .append(" and age is ")
                .append(age)
                .toString();
    }

    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (this == other) {
            return true;
        }

        if (other instanceof User) {
            User user = (User)other;
            return this.age == user.age && this.name.equals(user.name);
        } else {
            return false;
        }

    }
}

class Runner {
    // https://habr.com/ru/post/168195/

    public static void main(String[] args) {
        User user = new User(25, "Jhon");
        System.out.println(user);
        System.out.println(user.hashCode());
    }
}