package com.belushkin.lesson7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Iter {
}


class MyContainer<T> implements Iterable<T> {
    List<T> values;

    public MyContainer(T...values) {
        this.values = Arrays.asList(values);
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            int index = 0;

            @Override
            public boolean hasNext() {
                return index < values.size();
            }

            @Override
            public T next() {
                return values.get(index++);
            }
        };
    }
}
