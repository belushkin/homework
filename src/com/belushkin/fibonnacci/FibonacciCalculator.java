package com.belushkin.fibonnacci;

import java.util.HashMap;
import java.util.Stack;

public class FibonacciCalculator {

    private HashMap<Long, Long> map;

    public FibonacciCalculator() {
        map = new HashMap<>();
    }

    public long fib(long value) {
        if (value == 0 || value == 1) {
            return 1;
        } else if (map.containsKey(value)) {
            return map.get(value);
        } else {
            long fibValue = fib(value - 1) + fib(value - 2);
            map.put(value, fibValue);
            return fibValue;
        }
    }

    public long fib2(long value) {
        Stack<Long> stack  = new Stack<Long>();

        stack.push(value-2);
        stack.push(value-1);

        long res = 0;
        while (!stack.empty()) {
            long i = stack.pop();
            if (i <= 1) {
                res += 1;
            } else if (map.containsKey(value)) {
                return map.get(value);
            } else {
                stack.push(i-2);
                stack.push(i-1);
            }
        }
        return res;
    }

    public static void main(String[] args) {

        FibonacciCalculator calc = new FibonacciCalculator();
//        System.out.println(fib(35));
        System.out.println(calc.fib(55));
    }
}
