package com.belushkin.lesson5;

public enum Gender {
    MALE(new String[]{""}) {
        @Override
        void printNames(){
            // no-op
        }
    },
    FEMALE(new String[]{""}) {
        @Override
        void printNames(){
            // no-op
        }
    };

    private final String [] names;

    Gender(String[] names) {
        this.names = names;
    }

    abstract void printNames();

//    public Gender of(String gender) {
////        for (Gender gender: values()) {
////            if (gender.name().equalsIgnoreCase(gender.)) {
////                return 1;
////            }
////        }
//    }
}
