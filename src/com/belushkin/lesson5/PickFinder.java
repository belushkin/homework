package com.belushkin.lesson5;

public class PickFinder {

    private final PickSearcher pickSearcher;

    public PickFinder(PickSearcher pickSearcher){
        this.pickSearcher = pickSearcher;

    }

    public int pickSum(int[] picks) {
        int sum = 0;
        for (int pick : picks) {
            System.out.println(pickSearcher.search(pick));
            print();
            sum += pick;
        }
        return -1;
    }

    public void print(){
        System.out.println("pick touched");
    }

    //TODO: tests + implementation
    //TODO: implement faster than O(n) -> O(logn)
    public int findPick(int[] picks) {
        if (picks.length == 0) {
            return -1;
        } else {
            return 0;
        }
//        if (picks[0] > )
//        return -1;
    }
}
