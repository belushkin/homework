package com.belushkin.hw7.pet;

import com.belushkin.hw7.Species;

public class Fish extends Pet {

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Fish() {
        super();
    }

    public void respond() {
        if (getNickname() != null) {
            String message = "Hello host. I am " +
                    getNickname() +
                    ". I am fish";
            System.out.println(message);
        }
    }

}
