package com.belushkin.hw7.pet;

public class Mouse extends Pet{

    public void respond() {
    }

    public Mouse(String nickname) {
        super(nickname);
    }

    public Mouse(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Mouse() {
        super();
    }
}
