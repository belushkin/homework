package com.belushkin.hw7.pet;

import com.belushkin.hw7.Species;

public class Dog extends Pet implements FoulInterface{

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Dog() {
        super();
    }

    public void respond() {
        if (getNickname() != null) {
            String message = "Hello host. I am " +
                    getNickname() +
                    ". I am dog";
            System.out.println(message);
        }
    }

    public void foul() {
        if (getNickname() != null) {
            System.out.println("Playing in the garden...");
        }
    }

}
