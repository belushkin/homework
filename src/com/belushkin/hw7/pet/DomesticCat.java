package com.belushkin.hw7.pet;

import com.belushkin.hw7.Species;

public class DomesticCat extends Pet implements FoulInterface{

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    public DomesticCat() {
        super();
    }

    public void respond() {
        if (getNickname() != null) {
            String message = "Hello host. I am " +
                    getNickname() +
                    ". I am domestic cat";
            System.out.println(message);
        }
    }

    public void foul() {
        if (getNickname() != null) {
            System.out.println("Sleeping on the PC...");
        }
    }

}
