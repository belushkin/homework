package com.belushkin.hw7;

import com.belushkin.hw7.human.Human;

public class Main {

    static {
        System.out.println(Main.class.getName() + " is loading");
    }

    {
        System.out.println(Main.class.getName() + " was created");
    }

    public static void main(String[] args) {

        for (int i = 0; i < 100000; i++) {

            String[][] schedule = {
                    {DayOfWeek.Monday.name(), "walk in a park"},
                    {DayOfWeek.Tuesday.name(), "go to school"},
                    {DayOfWeek.Wednesday.name(), "watch a film"},
                    {DayOfWeek.Thursday.name(), "have a rest"},
                    {DayOfWeek.Friday.name(), "visit friends"},
            };

            Human human = new Human(
                    "name",
                    "surname",
                    1987,
                    99,
                    schedule);
        }

    }

}
