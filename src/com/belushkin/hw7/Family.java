package com.belushkin.hw7;

import com.belushkin.hw7.human.Human;
import com.belushkin.hw7.human.Man;
import com.belushkin.hw7.human.Woman;
import com.belushkin.hw7.pet.Pet;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Family {

    private Human father;
    private Human mother;
    private Human[] children;
    private Pet pet;
    private int TRICK_COMPARE_LIMIT = 100;
    private int count = 0;

    static {
        System.out.println(Family.class.getName() + " is loading");
    }

    {
        System.out.println(Family.class.getName() + " was created");
    }

    public Family(Man father, Woman mother, Pet pet, Human... children) {

        this.father = father;
        this.father.setFamily(this);

        this.mother = mother;
        this.mother.setFamily(this);

        this.pet = pet;
        this.children = children;
        for (Human child : this.children) {
            child.setFamily(this);
        }
        this.count = this.children.length;
    }

    public void addChild(Human child) {
        if (children.length*0.8 <= count) {
            extend();
        }

        child.setFamily(this);
        children[count++] = child;
    }

    public boolean deleteChild(Human child) {
        for (int i = 0; i < children.length; i++) {
            if (child.equals(children[i])) {
                return deleteChild(i);
            }
        }
        return false;
    }

    public boolean deleteChild(int index) {
        if (children == null
                || index < 0
                || index >= children.length) {

            return false;
        }

        Human[] anotherArray = new Human[children.length - 1];
        for (int i = 0, k = 0; i < children.length; i++) {
            if (i == index) {
                continue;
            }

            anotherArray[k++] = children[i];
        }

        children = anotherArray;
        count--;
        return true;
    }

    public Pet getPet() {
        return pet;
    }

    public Human getFather() {
        return father;
    }

    public Human getMother() {
        return mother;
    }

    public int countFamily() {
        return count + 2;
    }

    public void describePet() {
        if (pet != null && pet.getSpecies() != null) {
            StringBuilder description = new StringBuilder("I have a ").
                    append(pet.getSpecies());

            if (pet.getAge() > 0) {
                description.
                        append(", it is ").
                        append(pet.getAge()).
                        append(" years");
            }

            if (pet.getTrickLevel() > 0) {
                description.
                        append(", it is ").
                        append(pet.getTrickLevelAsString());
            }
            System.out.println(description.toString());
        }
    }

    public boolean feedPet(boolean isTimeToFeed) {
        if (pet == null || pet.getNickname() == null) {
            return false;
        }

        if (isTimeToFeed) {
            System.out.println("Hm... Let's feed " + pet.getNickname());
            pet.eat();
            return true;
        } else {
            Random random = new Random();
            int trickCompareNumber = random.nextInt(TRICK_COMPARE_LIMIT);
            if (pet.getTrickLevel() > trickCompareNumber) {
                System.out.println("Hm... Let's feed " + pet.getNickname());
                pet.eat();
                return true;
            }
        }

        System.out.println("I think " + pet.getNickname() + " is not hungry.");
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;

        if (father == null && family.father != null) {
            return false;
        }
        if (mother == null && family.mother != null) {
            return false;
        }
        if (pet == null && family.pet != null) {
            return false;
        }

        return TRICK_COMPARE_LIMIT == family.TRICK_COMPARE_LIMIT &&
                count == family.count &&
                (father == null || father.equals(family.father)) &&
                (mother == null || mother.equals(family.mother)) &&
                Arrays.equals(children, family.children) &&
                (pet == null || pet.equals(family.pet));
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(father, mother, pet, TRICK_COMPARE_LIMIT, count);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder family = new StringBuilder("Family{");
        family.
                append("father='").
                append(father).
                append("', ");

        family.
                append("mother='").
                append(mother).
                append("', ");

        if (children != null && children.length > 0) {
            family.
                    append("children='").
                    append(Arrays.toString(narrow(children))).
                    append("', ");
        }

        if (pet != null) {
            family.
                    append("pet='").
                    append(pet).
                    append("}");

        } else {
            family.
                    append("}");
        }

        if (family.charAt(family.length()-3) == ',') {
            family.deleteCharAt(family.length()-3);
            family.deleteCharAt(family.length()-2);
        }
        return family.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Garbage Collector, family: " + this);
    }

    private void extend() {
        int length = (children.length == 0) ? 1 : children.length;
        Human[] arrayCopy = new Human[length * 2];
        System.arraycopy(children, 0, arrayCopy, 0, children.length);
        children = arrayCopy;
    }

    private Human[] narrow(Human[] children) {
        Human[] arrayCopy = new Human[count];
        System.arraycopy(children, 0, arrayCopy, 0, count);
        return arrayCopy;
    }

}
