package com.belushkin.hw7;

public enum Species {
    DomesticCat(false, 4, true),
    Dog(false, 4, true),
    RoboCat(false, 4, false),
    Fish(true, 0, false),
    Unknown (false, 0, false);

    private final boolean canFly;
    private final int numberOfLegs;
    private final boolean hasFur;


    Species(boolean canFly, int numberOfLegs, boolean hasFur) {

        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    public boolean isCanFly() {
        return canFly;
    }

    public boolean isHasFur() {
        return hasFur;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

}
