package com.belushkin.binary;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Solver {

    private ArrayList<Integer> items = new ArrayList<>();
    private ArrayList<Integer> values = new ArrayList<>();
    private ArrayList<ArrayList<Integer>> ways = new ArrayList<>();
    private int counter = 0;
    private int iterator = 0;
    private final static int S = 7;
    private final static String F = "/home/oht/workspace/homework/src/com/belushkin/binary/examples/input_10b.txt";

    public static void main(String[] args) {
        Solver solver = new Solver();
        solver.readTree();
        solver.doit();
    }

    private void doit() {
        // 1
        inorderTreeWalk(0, false);
        Collections.sort(values);

        // 2
        counter = 0;
        inorderTreeWalk(0, true);
        System.out.println(items);

        // 3
        counter = 0;
        preorderTreeWalk(0, new ArrayList<ArrayList<Integer>>());
        ways.forEach(System.out::println);
    }

    private void inorderTreeWalk(int i, Boolean replace) {

        if (items.get(i) != 0) {
            inorderTreeWalk(left(i), replace);
            if (replace) {
                items.set(i, values.get(iterator++));
            } else {
                values.add(items.get(i));
            }
            inorderTreeWalk(right(i), replace);
        }
    }

    private void preorderTreeWalk(int i, ArrayList<ArrayList<Integer>> a) {

        if (items.get(i) != 0) {

            ArrayList<ArrayList<Integer>> b = c(a);

            b.forEach(integers -> integers.add(items.get(i)));
            b.add(new ArrayList<>() {
                {
                    add(items.get(i));
                }
            });

            b.forEach(integers -> {
                Optional<Integer> sum = integers.stream().reduce(Integer::sum);
                sum.ifPresent((Integer integer) -> {
                    if (integer != S) return;
                    ways.add(integers);
                });
            });

            preorderTreeWalk(left(i), b);
            preorderTreeWalk(right(i), b);
        }
    }

    private int left(int i) {
        return ++counter;
    }

    private int right(int i) {
        return ++counter;
    }

    private void readTree() {
        try
        {
            File file=new File(F);
            FileReader fr=new FileReader(file);
            BufferedReader br=new BufferedReader(fr);
            String line = br.readLine();

            List<String> stringItems = Arrays.asList(line.split(" "));

            items.addAll(
                    stringItems.
                            stream().
                            map(Integer::valueOf).
                            collect(Collectors.toList())
            );
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<ArrayList<Integer>> c(ArrayList<ArrayList<Integer>> a) {

        ArrayList<ArrayList<Integer>> b = new ArrayList<>();
        for (ArrayList<Integer> integers : a) {
            ArrayList<Integer> d = new ArrayList<>(integers);
            b.add(d);
        }
        return b;
    }
}

