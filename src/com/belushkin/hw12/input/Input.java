package com.belushkin.hw12.input;

import com.belushkin.hw12.Family;
import com.belushkin.hw12.FamilyController;
import com.belushkin.hw12.human.*;

import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Input {

    private Scanner scanner;

    private String command = "";

    private final FamilyController controller;

    private SubInput subInput;

    private final static int FAMILY_BOUND = 5;

    public Input(FamilyController controller) {
        this.controller = controller;
        this.scanner = new Scanner(System.in);
        this.subInput = new SubInput();
    }

    public String getCommand() {

        List<Family> families;
        String input = scanner.nextLine().toLowerCase();

        switch (input) {
            case "1":

                controller.deleteAllFamilies();

                Random random = new Random();
                int bound = random.nextInt(FAMILY_BOUND) + 1;

                FamilyCreator familyCreator = new FamilyCreator(controller);
                for (int i = 0; i < bound; i++) {
                    familyCreator.create();
                }
                break;
            case "2":

                controller.displayAllFamilies();
                break;
            case "3":

                System.out.println("Отобразить список семей, где количество людей больше заданного");
                System.out.println("Запросить интересующее число:");

                families = controller.getFamiliesBiggerThan(
                    subInput.getIntInput()
                );
                if (families.size() > 0) {
                    controller.displayFamilies(families);
                } else {
                    System.out.println("No such families");
                }

                break;
            case "4":

                System.out.println("Отобразить список семей, где количество людей меньше заданного");
                System.out.println("Запросить интересующее число:");

                families = controller.getFamiliesLessThan(
                        subInput.getIntInput()
                );
                if (families.size() > 0) {
                    controller.displayFamilies(families);
                } else {
                    System.out.println("No such families");
                }

                break;
            case "5":

                System.out.println("Подсчитать количество семей, где количество членов равно");
                System.out.println("Запросить интересующее число:");

                int amount = controller.countFamiliesWithMemberNumber(subInput.getIntInput());
                System.out.println("Found families with such number of members: " + amount);

                break;
            case "6":

                System.out.println("Создать новую семью");

                System.out.println("запросить имя матери:");
                String motherName = subInput.getStringInput();

                System.out.println("запросить фамилию матери:");
                String motherSurname = subInput.getStringInput();

                System.out.println("запросить год рождения матери:");
                int motherBirthYear = subInput.getIntInputYear();

                System.out.println("запросить месяц рождения матери:");
                int motherBirthMonth = subInput.getIntInputMonth();

                System.out.println("запросить день рождения матери:");
                int motherBirthDay = subInput.getIntInputDay(motherBirthYear, motherBirthMonth);

                System.out.println("запросить iq матери:");
                int motherIq = subInput.getIntInput();

                System.out.println("запросить имя отца:");
                String fatherName = subInput.getStringInput();

                System.out.println("запросить фамилию отца:");
                String fatherSurname = subInput.getStringInput();

                System.out.println("запросить год рождения отца:");
                int fatherBirthYear = subInput.getIntInputYear();

                System.out.println("запросить месяц рождения отца:");
                int fatherBirthMonth = subInput.getIntInputMonth();

                System.out.println("запросить день рождения отца:");
                int fatherBirthDay = subInput.getIntInputDay(motherBirthYear, motherBirthMonth);

                System.out.println("запросить iq отца:");
                int fatherIq = subInput.getIntInput();

                String fatherBirthday = String.valueOf(fatherBirthDay) +
                        '/' +
                        fatherBirthMonth +
                        '/' +
                        fatherBirthYear;

                Man father = new Man(
                        fatherName,
                        fatherSurname,
                        fatherBirthday,
                        fatherIq,
                        null
                );

                String motherBirthday = String.valueOf(motherBirthDay) +
                        '/' +
                        motherBirthMonth +
                        '/' +
                        motherBirthYear;

                Woman mother = new Woman(
                        motherName,
                        motherSurname,
                        motherBirthday,
                        motherIq,
                        null
                );

                controller.createNewFamily(father, mother);
                break;
            case "7":

                System.out.println("Удалить семью по индексу семьи в общем списке");
                System.out.println("запросить порядковый номер семьи (ID):");

                int indexOfFamilyToRemove = subInput.getIntInputFamilyIndex(
                        controller.getAllFamilies().size()
                );
                controller.deleteFamily(indexOfFamilyToRemove);
                break;
            case "8":

                System.out.println("Редактировать семью по индексу семьи в общем списке");
                System.out.println(SubMenu.SUB_MENU);
                int subMenuInput = subInput.getIntSubInput();
                switch (subMenuInput) {
                    case 1:
                        System.out.println("запросить порядковый номер семьи (ID)");
                        int indexOfFamilyToBorn = subInput.getIntInputFamilyIndex(
                                controller.getAllFamilies().size()
                        );

                        System.out.println("запросить необходимые данные (какое имя дать мальчику)");
                        String boyName = subInput.getStringInput();

                        System.out.println("запросить необходимые данные (какое имя дать девочке)");
                        String girlName = subInput.getStringInput();

                        controller.bornChild(
                                controller.getFamilyById(indexOfFamilyToBorn),
                                boyName,
                                girlName
                        );
                        break;
                    case 2:

                        System.out.println("запросить порядковый номер семьи (ID)");
                        int indexOfFamilyToAdopt = subInput.getIntInputFamilyIndex(
                                controller.getAllFamilies().size()
                        );

                        System.out.println("запросить имя ребенка:");
                        String childName = subInput.getStringInput();

                        System.out.println("запросить фамилию ребенка:");
                        String childSurname = subInput.getStringInput();

                        System.out.println("запросить год рождения ребенка:");
                        int childBirthYear = subInput.getIntInputYear();

                        System.out.println("запросить месяц рождения ребенка:");
                        int childBirthMonth = subInput.getIntInputMonth();

                        System.out.println("запросить день рождения ребенка:");
                        int childBirthDay = subInput.getIntInputDay(childBirthYear, childBirthMonth);

                        System.out.println("запросить iq ребенка:");
                        int childIq = subInput.getIntInput();

                        String childBirthday = String.valueOf(childBirthDay) +
                                '/' +
                                childBirthMonth +
                                '/' +
                                childBirthYear;

                        Human child = new Human(
                                childName,
                                childSurname,
                                childBirthday,
                                childIq,
                                null
                        );
                        controller.adoptChild(
                                controller.getFamilyById(indexOfFamilyToAdopt),
                                child
                        );
                        break;
                    case 3:
                        System.out.println("Back to the main menu");
                        break;
                    default:
                        break;
                }
                break;
            case "9":

                System.out.println("Удалить всех детей старше возраста (во всех семьях удаляются дети старше указанного возраста - будем считать, что они выросли)");
                System.out.println("запросить интересующий возраст:");
                int removeChildAge = subInput.getIntInput();
                controller.deleteAllChildrenOlderThen(removeChildAge);
                break;
            case "10":
                command = "exit";
                break;
            default:
                System.out.println(Menu.MENU);
                break;
        }
        return command;
    }

}
