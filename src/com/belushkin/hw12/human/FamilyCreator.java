package com.belushkin.hw12.human;

import com.belushkin.hw12.FamilyController;

public class FamilyCreator {

    private FamilyController controller;

    public FamilyCreator(FamilyController controller) {

        this.controller = controller;
    }

    public void create() {
        controller.createNewFamily(
                new Man(
                        new NameGenerator(1).getName(),
                        new NameGenerator(1).getSurname(),
                        "12/07/2001"),
                new Woman(
                        new NameGenerator(0).getName(),
                        new NameGenerator(0).getSurname(),
                        "07/09/2000")
        );
    }
}
