package com.belushkin.hw12.human;

import java.util.Map;

public final class Man extends Human {

    public Man(String name, String surname, String birthDayString) {
        super(name, surname, birthDayString);
    }

    public Man(String name, String surname, String birthDayString, int iq, Map<String, String> schedule) {
        super(name, surname, birthDayString, iq, schedule);
    }

    public Man() {
        super();
    }

    @Override
    public void greetPet() {
        if (getFamily().getPet() != null && getFamily().getPet().getNickname() != null) {
            System.out.println("Welcome " + getFamily().getPet().getNickname());
        }
    }

    public String repairCar() {
        return "I am repairing car";
    }

    public String prettyFormat() {
        return "man: " + this.toString().substring(3);
    }

    public String prettyFormatFather() {
        return "father: " + this.toString().substring(3);
    }

    public String prettyFormatChild() {
        return "boy: " + this.toString().substring(3);
    }

}
