package com.belushkin.hw12;


import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> families = new ArrayList<>();

    public List<Family> getAllFamilies() {
        return families;
    }

    public Family getFamilyByIndex(int index) {
        if(index >= families.size() || index < 0){
            return null;
        }
        return families.get(index);
    }

    public boolean deleteFamily(int index) {
        if(index >= families.size() || index < 0){
            return false;
        }
        families.remove(index);
        return true;
    }

    public boolean deleteFamily(Family family) {
        return families.remove(family);
    }

    public void deleteAllFamilies() {
        families = new ArrayList<>();
    }

    public void saveFamily(Family family) {
        int index = families.indexOf(family);
        if (index == -1) {
            families.add(family);
        } else {
            families.set(index, family);
        }
    }

}
