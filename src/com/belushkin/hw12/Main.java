package com.belushkin.hw12;


import com.belushkin.hw12.input.Input;
import com.belushkin.hw12.input.Menu;

public class Main {

    static {
        System.out.println(Main.class.getName() + " is loading");
    }

    {
        System.out.println(Main.class.getName() + " was created");
    }

    public static void main(String[] args) {

        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        FamilyController familyController = new FamilyController(familyService);

        Input input = new Input(familyController);
        System.out.println(Menu.MENU);
        System.out.println("Please enter number from menu: ");
        String command = input.getCommand();

        while (!command.equals("exit")) {
            System.out.println("\nPlease enter number from menu: ");
            command = input.getCommand();
        }
    }

}
