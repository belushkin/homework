package com.belushkin.hw12.pet;

import java.util.Set;

public class DomesticCat extends Pet implements FoulInterface {

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public DomesticCat() {
        super();
    }

    public void respond() {
        if (getNickname() != null) {
            String message = "Hello host. I am " +
                    getNickname() +
                    ". I am domestic cat";
            System.out.println(message);
        }
    }

    public void foul() {
        if (getNickname() != null) {
            System.out.println("Sleeping on the PC...");
        }
    }

}
