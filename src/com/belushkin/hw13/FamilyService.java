package com.belushkin.hw13;

import com.belushkin.hw13.exceptions.FamilyOverflowException;
import com.belushkin.hw13.human.Human;
import com.belushkin.hw13.human.Man;
import com.belushkin.hw13.human.Woman;
import com.belushkin.hw13.io.FileWorker;
import com.belushkin.hw13.pet.Pet;

import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FamilyService {

    private FamilyDao familyDao;

    private String filename = "hw13.data";

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public FamilyService(FamilyDao familyDao, String filename) {
        this.familyDao = familyDao;
        if (filename != null) {
            this.filename = filename;
        }
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public Family getFamilyByIndex(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public boolean deleteFamily(int index) {
        return familyDao.deleteFamily(index);
    }

    public boolean deleteFamily(Family family) {
        return familyDao.deleteFamily(family);
    }

    public void deleteAllFamilies() {
        familyDao.deleteAllFamilies();
    }

    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

    public void displayAllFamilies() {
        displayFamilies(familyDao.getAllFamilies());
    }

    public void displayFamilies(List<Family> families) {
        List<String> collect = IntStream.range(0, families.size())
                .mapToObj(index -> index+1 + ") " + families.get(index).prettyFormat())
                .collect(Collectors.toList());

        collect.forEach(System.out::println);
    }

    public void saveData(List<Family> families) {

        FileWorker.serialize(filename, families);
    }

    public void loadData(List<Family> families) {

        familyDao.loadData(families);
    }

    public ArrayList<Family> prepareData() {
        return FileWorker.deserialize(filename);
    }

    public List<Family> getFamiliesBiggerThan(int amount) {

        return familyDao.
                getAllFamilies().
                stream().
                filter(family -> family.countFamily() > amount).
                collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int amount) {
        return familyDao.
                getAllFamilies().
                stream().
                filter(family -> family.countFamily() < amount).
                collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int amount) {
        return (int) familyDao.
                getAllFamilies().
                stream().
                filter(family -> family.countFamily() == amount).
                count();
    }

    public Family createNewFamily(Man man, Woman woman) {
        Family family = new Family(
                man,
                woman,
                null
        );
        familyDao.saveFamily(family);
        return family;
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public void bornChild(Family family, String maleName, String femaleName) {
        Woman mother = family.getMother();

        Human child = mother.bornChild();
        if (child.getClass().getSimpleName().equals("Man")) {
            child.setName(maleName);
        } else {
            child.setName(femaleName);
        }
    }

    public Family adoptChild(Family family, Human... child) {
        for (Human human : child) {
            int FAMILY_LIMIT_BOUND = 6;
            if (family.countFamily() < FAMILY_LIMIT_BOUND) {
                family.addChild(human);
            } else {
                throw new FamilyOverflowException("Family size is more than family limit");
            }
        }
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyDao.
                getAllFamilies().
                forEach(
                        family -> family.
                                getChildren().
                                removeIf(child -> child.getYearsOfLife() > age)
                );
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int familyIndex) {
        Family family = getFamilyByIndex(familyIndex);
        if (family != null) {
            return family.getPets();
        }
        return Collections.emptySet();
    }

    public void addPet(int familyIndex, Pet... animal) {
        Family family = getFamilyByIndex(familyIndex);
        if (family != null) {
            family.addPet(animal);
        }
    }
}
