package com.belushkin.hw13;

import com.belushkin.hw13.exceptions.FamilyOverflowException;
import com.belushkin.hw13.human.Human;
import com.belushkin.hw13.human.Man;
import com.belushkin.hw13.human.Woman;
import com.belushkin.hw13.io.FileWorker;
import com.belushkin.hw13.io.Logger;
import com.belushkin.hw13.pet.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public Family getFamilyByIndex(int index) {
        return familyService.getFamilyByIndex(index);
    }

    public boolean deleteFamily(int index) {
        return familyService.deleteFamily(index);
    }

    public boolean deleteFamily(Family family) {
        return familyService.deleteFamily(family);
    }

    public void deleteAllFamilies() {
        familyService.deleteAllFamilies();
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public void displayAllFamilies() {
        familyService.displayFamilies(familyService.getAllFamilies());
    }

    public void displayFamilies(List<Family> families) {
        familyService.displayFamilies(families);
    }

    public void saveData(List<Family> families) {
        familyService.saveData(families);
    }

    public void loadData() {
        familyService.loadData(
                familyService.prepareData()
        );
    }

    public List<Family> getFamiliesBiggerThan(int amount) {
        return familyService.getFamiliesBiggerThan(amount);
    }

    public List<Family> getFamiliesLessThan(int amount) {
        return familyService.getFamiliesLessThan(amount);
    }

    public int countFamiliesWithMemberNumber(int amount) {
        return familyService.countFamiliesWithMemberNumber(amount);
    }

    public Family createNewFamily(Man man, Woman woman) {
        return familyService.createNewFamily(man, woman);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamily(index);
    }

    public void bornChild(Family family, String maleName, String femaleName) {
        familyService.bornChild(family, maleName, femaleName);
    }

    public Family adoptChild(Family family, Human... child) {
        try {
            return familyService.adoptChild(family, child);
        } catch (FamilyOverflowException e) {
            Logger.error("adopt child exception");
            return family;
        }
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet... animal) {
        familyService.addPet(familyIndex, animal);
    }

}
