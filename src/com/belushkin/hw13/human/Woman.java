package com.belushkin.hw13.human;

import java.util.Map;
import java.util.Random;

public final class Woman extends Human implements HumanCreator {

    public Woman(String name, String surname, String birthDayString) {
        super(name, surname, birthDayString);
    }

    public Woman(String name, String surname, String birthDayString, int iq, Map<String, String> schedule) {
        super(name, surname, birthDayString, iq, schedule);
    }

    public Woman() {
        super();
    }

    @Override
    public void greetPet() {
        if (getFamily().getPet() != null && getFamily().getPet().getNickname() != null) {
            System.out.println("Nice to meet you " + getFamily().getPet().getNickname());
        }
    }

    public String makeup() {
        return "I am making up";
    }

    @Override
    public Human bornChild() {
        Random random = new Random();
        Human child;

        int sex = random.nextInt(1);
        String name = Names.values()[random.nextInt(Names.values().length)].name();
        String surname = getFamily().getFather().getSurname();
        int iq = (getFamily().getFather().getIq() + getFamily().getMother().getIq()) / 2;

        if (sex == 0) {
            child = new Man(name, surname,"", iq,null);
        } else {
            child = new Woman(name, surname,"", iq,null);
        }
        getFamily().addChild(child);
        return child;
    }

    public String prettyFormat() {
        return "woman: " + this.toString().substring(5);
    }

    public String prettyFormatMother() {
        return "mother: " + this.toString().substring(5);
    }

    public String prettyFormatChild() {
        return "girl: " + this.toString().substring(5);
    }

}
