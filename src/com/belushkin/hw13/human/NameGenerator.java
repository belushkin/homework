package com.belushkin.hw13.human;

import java.util.Random;

public class NameGenerator {


    public String name;
    public String surname;

    public NameGenerator(int sex) {

        Random random = new Random();
        if (sex == 1) {
            this.name = Names.values()[
                    random.nextInt(Names.values().length)
                    ].name();

        } else {
            this.name = WomanNames.values()[
                    random.nextInt(WomanNames.values().length)
                    ].name();

        }

        this.surname = Surnames.values()[
                random.nextInt(Surnames.values().length)
                ].name();
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
}
