package com.belushkin.hw13.pet;

import java.io.Serializable;
import java.util.Set;

public class Mouse extends Pet implements Serializable {

    public void respond() {
    }

    public Mouse(String nickname) {
        super(nickname);
    }

    public Mouse(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Mouse() {
        super();
    }

}
