package com.belushkin.hw13.pet;

import com.belushkin.hw9.Species;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

public abstract class Pet implements Serializable {

    protected Species species;

    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;

    private final int TRICKNESS_LEVEL = 50;

    static {
        System.out.println(Pet.class.getName() + " is loading");
    }

    {
        System.out.println(Pet.class.getName() + " was created");
    }

    public Pet(String nickname){

        this.nickname = nickname;
        try {
            this.species = Species.valueOf(this.getClass().getSimpleName());
        } catch (Exception e) {
            this.species = Species.Unknown;
        }
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits){

        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;

        try {
            this.species = Species.valueOf(this.getClass().getSimpleName());
        } catch (Exception e) {
            this.species = Species.Unknown;
        }
    }

    public Pet(){
        try {
            this.species = Species.valueOf(this.getClass().getSimpleName());
        } catch (Exception e) {
            this.species = Species.Unknown;
        }
    }

    public String getNickname() {
        return nickname;
    }

    public Species getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String getTrickLevelAsString() {
        return getTrickLevel() > TRICKNESS_LEVEL ? "tricky" : "not tricky";
    }

    public void eat() {
        System.out.println("I am eating!");
    }

    public abstract void respond();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;

        if (species == null && pet.species != null) {
            return false;
        }
        if (nickname == null && pet.nickname != null) {
            return false;
        }
        if (habits == null && pet.habits != null) {
            return false;
        }

        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                (species == null || species.equals(pet.species)) &&
                (nickname == null || nickname.equals(pet.nickname)) &&
                (habits == null || habits.equals(pet.habits));
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel, habits, TRICKNESS_LEVEL);
    }

    public String prettyFormat() {

        StringBuilder pet = new StringBuilder("{");

        if (species != null) {
            pet.append("species='").
                    append(species.toString().toUpperCase()).
                    append("', ");
        }
        if (nickname != null) {
            pet.
                    append("nickname='").
                    append(nickname).
                    append("', ");
        }

        if (age > 0) {
            pet.
                    append("age='").
                    append(age).
                    append("', ");
        }

        if (trickLevel > 0) {
            pet.
                    append("trickLevel='").
                    append(trickLevel).
                    append("', ");
        }

        if (habits != null) {
            pet.
                    append("habits=[").
                    append(String.join(", ", habits)).
                    append("]}");
        } else {
            pet.
                    append("}");
        }

        if (pet.charAt(pet.length()-3) == ',') {
            pet.deleteCharAt(pet.length()-3);
            pet.deleteCharAt(pet.length()-2);
        }
        return pet.toString();
    }

    @Override
    public String toString() {
        if (species == null) {
            return "{}";
        }

        StringBuilder pet = new StringBuilder(species + "{");
        if (nickname != null) {
            pet.
                    append("nickname='").
                    append(nickname).
                    append("', ");
        }

        if (age > 0) {
            pet.
                    append("age='").
                    append(age).
                    append("', ");
        }

        if (trickLevel > 0) {
            pet.
                    append("trickLevel='").
                    append(trickLevel).
                    append("', ");
        }

        pet.
                append("properties=[").
                append("'can fly'=").append(species.isCanFly()).
                append(",").
                append("'has fur'=").append(species.isHasFur()).
                append(",").
                append("'number of legs'=").append(species.getNumberOfLegs()).
                append("], ");

        if (habits != null) {
            pet.
                    append("habits=[").
                    append(String.join(", ", habits)).
                    append("]}");
        } else {
            pet.
                    append("}");
        }

        if (pet.charAt(pet.length()-3) == ',') {
            pet.deleteCharAt(pet.length()-3);
            pet.deleteCharAt(pet.length()-2);
        }
        return pet.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Garbage Collector, pet: " + this);
    }

}
