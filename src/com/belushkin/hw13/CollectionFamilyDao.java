package com.belushkin.hw13;


import com.belushkin.hw13.io.FileWorker;
import com.belushkin.hw13.io.Logger;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> families = new ArrayList<>();

    public void loadData(List<Family> families) {
        this.families = families;
    }

    public List<Family> getAllFamilies() {

        Logger.info("get all families");
        return families;
    }

    public Family getFamilyByIndex(int index) {

        Logger.info("get family by index: " + index);
        if(index >= families.size() || index < 0){
            return null;
        }
        return families.get(index);
    }

    public boolean deleteFamily(int index) {

        Logger.info("delete family by index: " + index);
        if(index >= families.size() || index < 0){
            return false;
        }
        families.remove(index);
        return true;
    }

    public boolean deleteFamily(Family family) {

        Logger.info("delete family by instance");
        return families.remove(family);
    }

    public void deleteAllFamilies() {

        Logger.info("delete all families");
        families = new ArrayList<>();
    }

    public void saveFamily(Family family) {

        Logger.info("save family");
        int index = families.indexOf(family);
        if (index == -1) {
            families.add(family);
        } else {
            families.set(index, family);
        }
    }

}
