package com.belushkin.hw13.io;

import com.belushkin.hw13.Family;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileWorker {


    public static void serialize(String fileName, List<Family> families) {

        try {
            FileOutputStream fos = new FileOutputStream(getBasePath() + fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(families);
            oos.close();
            fos.close();
        } catch (IOException ioe) {
            Logger.error("serialize exception");
            ioe.printStackTrace();
        }
    }

    public static ArrayList<Family> deserialize(String fileName) {

        ArrayList<Family> families = new ArrayList<>();

        try {
            FileInputStream fis = new FileInputStream(getBasePath() + fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);

            families = (ArrayList) ois.readObject();

            ois.close();
            fis.close();
        } catch (IOException ioe) {
            Logger.error("io exception");
            ioe.printStackTrace();
            return families;
        } catch (ClassNotFoundException c) {
            Logger.error("class not found exception");
            System.out.println("Class not found");
            c.printStackTrace();
            return families;
        }

        return families;
    }

    private static String getBasePath() {
        return System.getProperty("user.dir") + "/storage/";
    }

}
