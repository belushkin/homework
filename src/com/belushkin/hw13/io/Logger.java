package com.belushkin.hw13.io;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {

    private static String filename = "application.log";

    public static void info(String message) {

        write(filename, getFormattedLogDateTime() + " [DEBUG] " + message);
    }

    public static void error(String message) {

        write(filename, getFormattedLogDateTime() + " [ERROR] " + message);
    }

    private static String getFormattedLogDateTime() {
        LocalDateTime now = LocalDateTime.now();
        String DATE_PATTERN = "dd/MM/yyyy HH:mm";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
        return now.format(formatter);
    }

    public static void write(String fileName, String text) {

        try {
            PrintWriter printWriter = new PrintWriter(
                    new PrintStream(
                            new FileOutputStream(getBasePath() + fileName, true)
                    )
            );
            printWriter.write("\n" + text);
            printWriter.close();
        } catch(IOException e) {
            error("io exception");
            throw new RuntimeException(e);
        }
    }

    public static String read(String fileName) throws FileNotFoundException {

        File file = new File(getBasePath() + fileName);
        StringBuilder sb = new StringBuilder();

        if (!file.exists()) {
            throw new FileNotFoundException(file.getName());
        }

        try {
            try (BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()))) {
                String s;
                while ((s = in.readLine()) != null) {
                    sb.append(s);
                    sb.append("\n");
                }
            }
        } catch(IOException e) {
            error("io exception");
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

    private static String getBasePath() {
        return System.getProperty("user.dir") + "/storage/";
    }
}
