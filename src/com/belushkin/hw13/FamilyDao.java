package com.belushkin.hw13;


import java.util.List;

public interface FamilyDao {

    public void loadData(List<Family> families);

    public List<Family> getAllFamilies();

    public Family getFamilyByIndex(int index);

    public boolean deleteFamily(int index);

    public boolean deleteFamily(Family family);

    public void saveFamily(Family family);

    public void deleteAllFamilies();
}
