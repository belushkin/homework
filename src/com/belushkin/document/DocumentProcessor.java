package com.belushkin.document;

//Document(String value, int version, String author, long timestamp)

// DocumentProcessor
// int score(word, Document)
// int score(word, Document...)
// boolean equals(Document, Document)
//
// the cat
// the dog

public class DocumentProcessor
{

    int score(String word, Document doc)
    {
        return score(word, new Document[]{doc});
    }

    int score(String word, Document... args)
    {
        int score = 0;
        for (Document arg : args) {
            String text = arg.getText();
            if (text.equals("")) {
                continue;
            }

            int in = text.indexOf(word, 0);
            while (in != -1) {
                score++;
                in = text.indexOf(word, in + word.length());
            }
        }
        return score;
    }

    boolean equals(Document doc1, Document doc2)
    {
        return doc1.getText().equals(doc2.getText());
    }

    public static void main(String[] args)
    {
        Document doc1 = new Document("the cat the cat cat the");
        Document doc2 = new Document("what is the dog out of the dog and dog not dog");

        DocumentProcessor documentProcessor = new DocumentProcessor();

        System.out.println(documentProcessor.score("cat", doc1));
        System.out.println(documentProcessor.score("dog", doc2));

        System.out.println(documentProcessor.equals(doc1, doc2));
    }

}


class Document {

    public String text;

    public Document(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
