package com.belushkin.lesson6;

public class App01 {
}



// From protected we spread to public in child
//
abstract class Illness{
    protected abstract String getName();
}

class Angina extends Illness {

    @Override
    public String getName() {
        return null;
    }
}


/*
* Polymorphism
* - Overriding
* - Overloading
* - Runtime
* - Generics - Typed polymorphism
*
* */


class Things<T extends Number> {

    private T[] things;


    public static void main(String[] args) {
        new Things<Integer>();
    }

}