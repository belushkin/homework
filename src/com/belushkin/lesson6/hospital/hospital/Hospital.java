package com.belushkin.lesson6.hospital.hospital;

import com.belushkin.lesson6.hospital.illness.Illness;

public interface Hospital {

    Illness[] getIllness();
    String getName();
    int getEmployeesCount();

}
