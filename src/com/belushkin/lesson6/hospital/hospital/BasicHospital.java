package com.belushkin.lesson6.hospital.hospital;

import com.belushkin.lesson6.hospital.illness.Illness;

public class BasicHospital implements Hospital {
    @Override
    public Illness[] getIllness() {
        return new Illness[0];
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public int getEmployeesCount() {
        return 0;
    }
}
