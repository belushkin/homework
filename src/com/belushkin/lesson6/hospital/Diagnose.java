package com.belushkin.lesson6.hospital;

public enum Diagnose {
    GOOD,
    MEDIUM,
    BAD
}
