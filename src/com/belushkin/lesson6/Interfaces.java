package com.belushkin.lesson6;

public class Interfaces {

    // enum, class
    // annotations, interfaces
}


interface Animal {

    public final String NAME = "";
    public static final String NAME_2 = "";

    default public String getName() {
        return "classindra";
    }

    int getNumberOfLegs();

    static String type() {
        return "STATIC_TYPE";
    }
}

