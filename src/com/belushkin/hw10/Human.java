package com.belushkin.hw10;

import com.belushkin.hw9.Family;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class Human {

    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<String, String> schedule;
    private Family family;

    private final String DATE_PATTERN = "dd/MM/yyyy";

    static {
        System.out.println(Human.class.getName() + " is loading");
    }

    {
        System.out.println(Human.class.getName() + " was created");
    }

    public Human(String name, String surname, String birthDayString) throws ParseException {

        this.name = name;
        this.surname = surname;

        Date birthDay = new SimpleDateFormat(DATE_PATTERN).
                parse(birthDayString);

        this.birthDate = birthDay.getTime();
    }

    public Human(String name, String surname, String birthDayString, int iq, Map<String, String> schedule) throws ParseException {

        this.name = name;
        this.surname = surname;
        this.iq = iq;
        this.schedule = schedule;

        Date birthDay = new SimpleDateFormat(DATE_PATTERN).
                parse(birthDayString);

        this.birthDate = birthDay.getTime();
    }

    public Human() {

    }

    public int getIq() {
        return iq;
    }

    public String getSurname() {
        return surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public String describeAge() {

        LocalDate bday = getBirthDateLocalDate();
        LocalDate today = LocalDate.now();
        Period age = Period.between(bday, today);

        int years = age.getYears();
        int months = age.getMonths();
        int days = age.getDays();

        return "time of life: years = " + years + ", months = " + months + ", days = " + days;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    public void greetPet() {
        if (family.getPet() != null && family.getPet().getNickname() != null) {
            System.out.println("Hello " + family.getPet().getNickname());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;

        if (name == null && human.name != null) {
            return false;
        }
        if (surname == null && human.surname != null) {
            return false;
        }
        if (schedule == null && human.schedule != null) {
            return false;
        }

        return birthDate == human.birthDate
                && iq == human.iq &&
                (name == null || name.equals(human.name)) &&
                (surname == null || surname.equals(human.surname)) &&
                (schedule == null || schedule.equals(human.schedule));
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq, schedule);
    }

    @Override
    public String toString() {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
        StringBuilder human = new StringBuilder(this.getClass().getSimpleName() + "{");
        if (name != null) {
            human.
                    append("name='").
                    append(name).
                    append("', ");
        }

        if (surname != null) {
            human.
                    append("surname='").
                    append(surname).
                    append("', ");
        }

        if (birthDate > 0) {
            human.
                    append("birthdate='").
                    append(formatter.format(getBirthDateLocalDate())).
                    append("', ");
        }

        if (iq > 0) {
            human.
                    append("iq='").
                    append(iq).
                    append("', ");
        }

        if (schedule == null) {
            human.
                    append("}");

        } else {
            human.
                    append("schedule='").
                    append(schedule.toString()).
                    append("}");
        }

        if (human.charAt(human.length()-3) == ',') {
            human.deleteCharAt(human.length()-3);
            human.deleteCharAt(human.length()-2);
        }
        return human.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Garbage Collector, human: " + this);
    }

    private LocalDate getBirthDateLocalDate() {
        return Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
    }
}
