package com.belushkin.hw4;

import java.util.Random;

public class Family {

    private Human father;
    private Human mother;
    private Human son;
    private Human daughter;


    public Family(Human father, Human mother, Human son, Human daughter) {

        this.father = father;
        this.mother = mother;
        this.son = son;
        this.daughter = daughter;
    }

    public void print() {
        System.out.println("Family");

        System.out.print("Father: ");
        System.out.println(father);

        System.out.print("Mother: ");
        System.out.println(mother);

        System.out.print("Son: ");
        System.out.println(son);

        System.out.print("Daughter: ");
        System.out.println(daughter);
        System.out.println();
    }

    public void callMethods() {
        son.describePet();
        son.greetPet();

        Random random = new Random();

        if (son.getPet() != null) {
            son.getPet().respond();
            son.getPet().foul();
        }
        son.feedPet(random.nextBoolean());

        daughter.describePet();
        daughter.greetPet();

        if (daughter.getPet() != null) {
            daughter.getPet().respond();
            daughter.getPet().foul();
        }
        daughter.feedPet(random.nextBoolean());

        System.out.println();
    }
}
