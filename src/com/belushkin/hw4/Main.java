package com.belushkin.hw4;

public class Main {

    public static void main(String[] args) {
        firstFamily();
        secondFamily();
    }

    static void firstFamily() {
        Human father = new Human("Nat", "Wolf", 1984);
        Human mother = new Human("Margaret", "Qualley", 1985);

        Human son = new Human(
                "LaKeith",
                "Stanfield",
                2006,
                father,
                mother
        );

        String[] habits = {"eat", "sleep", "walk"};
        Pet dog = new Pet(
                "dog",
                "batman",
                3,
                56,
                habits
        );

        String[][] schedule = {
                {"1", "walk in a park"},
                {"2", "go to school"},
                {"3", "watch a film"},
                {"4", "have a rest"},
                {"5", "visit friends"},
        };
        Human daughter = new Human(
                "LaKeith",
                "Stanfield",
                2006,
                89,
                dog,
                father,
                mother,
                schedule
        );

        Family family = new Family(
                father,
                mother,
                son,
                daughter
        );
        family.print();
        family.callMethods();
    }

    static void secondFamily() {
        Human father = new Human("Shea", "Whigham", 1983);
        Human mother = new Human();

        Pet cat = new Pet("cat", "hollywood");
        String[][] sonSchedule = {
                {"1", "walk on the street"},
                {"2", "go to shop"},
                {"3", "watch a cartoon"},
                {"4", "work"},
                {"5", "visit grandma"},
        };
        Human son = new Human(
                "Timothy",
                "Olyphant",
                2005,
                88,
                cat,
                father,
                mother,
                sonSchedule
        );

        String[][] daughterSchedule = {
                {"1", "run"},
                {"2", "go to cinema"},
                {"3", "go to spa"},
                {"4", "program"},
                {"5", "drink beer"},
        };
        Human daughter = new Human(
                "Julia",
                "Butters",
                2003,
                86,
                new Pet(),
                father,
                mother,
                daughterSchedule
        );

        Family family = new Family(
                father,
                mother,
                son,
                daughter
        );
        family.print();
        family.callMethods();
    }
}
