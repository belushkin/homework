package com.belushkin.hw4;

import java.util.Arrays;
import java.util.Random;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human father;
    private Human mother;
    private String[][] schedule;

    private int TRICK_COMPARE_LIMIT = 100;

    public Human(String name, String surname, int year) {

        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human father, Human mother) {

        this.name = name;
        this.surname = surname;
        this.year = year;
        this.father = father;
        this.mother = mother;
    }

    public Human(String name, String surname, int year, int iq, Pet  pet, Human father, Human mother, String[][] schedule) {

        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.father = father;
        this.mother = mother;
        this.schedule = schedule;
    }

    public Human() {

    }

    public void greetPet() {
        if (pet != null && pet.getNickname() != null) {
            System.out.println("Hello " + pet.getNickname());
        }
    }

    public void describePet() {
        if (pet != null && pet.getSpecies() != null) {
            StringBuilder description = new StringBuilder("I have a ").
                    append(pet.getSpecies());

            if (pet.getAge() > 0) {
                description.
                        append(", it is ").
                        append(pet.getAge()).
                        append(" years");
            }

            if (pet.getTrickLevel() > 0) {
                description.
                        append(", it is ").
                        append(pet.getTrickLevelAsString());
            }
            System.out.println(description.toString());
        }
    }

    public boolean feedPet(boolean isTimeToFeed) {
        if (pet == null || pet.getNickname() == null) {
            return false;
        }

        if (isTimeToFeed) {
            System.out.println("Hm... Let's feed " + pet.getNickname());
            pet.eat();
            return true;
        } else {
            Random random = new Random();
            int trickCompareNumber = random.nextInt(TRICK_COMPARE_LIMIT);
            if (pet.getTrickLevel() > trickCompareNumber) {
                System.out.println("Hm... Let's feed " + pet.getNickname());
                pet.eat();
                return true;
            }
        }

        System.out.println("I think " + pet.getNickname() + " is not hungry.");
        return false;
    }

    public Pet getPet() {
        return pet;
    }

    @Override
    public String toString() {
        StringBuilder human = new StringBuilder("Human{");
        if (name != null) {
            human.
                    append("name='").
                    append(name).
                    append("', ");
        }

        if (surname != null) {
            human.
                    append("surname='").
                    append(surname).
                    append("', ");
        }

        if (year > 0) {
            human.
                    append("year='").
                    append(year).
                    append("', ");
        }

        if (iq > 0) {
            human.
                    append("iq='").
                    append(iq).
                    append("', ");
        }

        if (mother != null) {
            human.
                    append("mother='").
                    append(mother).
                    append("', ");
        }

        if (father != null) {
            human.
                    append("father='").
                    append(father).
                    append("', ");
        }

        if (schedule != null) {
            human.
                    append("schedule='").
                    append(Arrays.deepToString(schedule)).
                    append("', ");
        }

        if (pet == null) {
            human.
                    append("}");
        } else {
            human.
                    append("pet='").
                    append(pet.toString()).
                    append("}");
        }

        if (human.charAt(human.length()-3) == ',') {
            human.deleteCharAt(human.length()-3);
            human.deleteCharAt(human.length()-2);
        }
        return human.toString();
    }
}
