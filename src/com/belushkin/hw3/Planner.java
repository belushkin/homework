package com.belushkin.hw3;

public class Planner {

    String[][] schedule = {
            {"Sunday", "do home work"},
            {"Monday", "go to courses; watch a film"},
            {"Tuesday", "do home task"},
            {"Wednesday", "walk in a park"},
            {"Thursday", "play video games"},
            {"Friday", "meet a friend"},
            {"Saturday", "have a rest"},
    };

    public boolean isDayInSchedule(String day){
        for (String[] days : schedule) {
            if (days[0].toLowerCase().equals(day)) {
                return true;
            }
        }
        return false;
    }

    public String getTaskByDay(String day) {
        for (String[] days : schedule) {
            if (days[0].toLowerCase().equals(day)) {
                return days[1];
            }
        }
        return "";
    }

    public String getDayByDay(String day) {
        for (String[] days : schedule) {
            if (days[0].toLowerCase().equals(day)) {
                return days[0];
            }
        }
        return "";
    }

    public void setTaskByDay(String day, String task) {
        for (String[] days : schedule) {
            if (days[0].toLowerCase().equals(day)) {
                days[1] = task;
            }
        }
    }
}
