package com.belushkin.hw3;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Input {

    private Scanner scanner;

    private String command = "";

    private final Planner planner;

    public Input(Planner planner) {
        this.planner = planner;
        this.scanner = new Scanner(System.in);
    }

    public String getCommand() {
        System.out.println("Please, input the day of the week:");
        String input = scanner.nextLine().toLowerCase();

        switch (input) {
            case "monday":
            case "tuesday":
            case "wednesday":
            case "thursday":
            case "friday":
            case "saturday":
            case "sunday":
                System.out.println("Your tasks for " +
                        planner.getDayByDay(input) +
                        ": " +
                        planner.getTaskByDay(input)
                );
                break;
            case "exit":
                command = "exit";
                break;
            default:
                Matcher matcher = Pattern.compile("(change|reschedule) ([\\w]*)").matcher(input);
                if (matcher.matches() && planner.isDayInSchedule(matcher.group(2))) {
                    String dayOfTheWeek = matcher.group(2);
                    System.out.println("Please, input new tasks for " + planner.getDayByDay(dayOfTheWeek) + ".");
                    String task = scanner.nextLine();
                    planner.setTaskByDay(dayOfTheWeek, task);
                } else {
                    System.out.println("Sorry, I don't understand you, please try again.");
                }
                break;
        }
        return command;
    }

}
