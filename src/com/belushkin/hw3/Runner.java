package com.belushkin.hw3;

public class Runner {

    public static void main(String[] args) {

        Input input = new Input(new Planner());
        String command = input.getCommand();

        while (!command.equals("exit")) {
            command = input.getCommand();
        }
    }
}
