package com.belushkin.lesson9;

import java.io.Closeable;
import java.io.IOException;
import java.nio.BufferOverflowException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CloseableRepository<T>  implements Closeable {

    private final List<T> items;
    private static final int MAX_ITEMS = 10;

    public CloseableRepository() {
        this.items = new ArrayList<>(MAX_ITEMS);
    }

    public int size() {
        return items.size();
    }

    @Override
    public void close() {
        System.out.println(items);
        if (items.size() == MAX_ITEMS) {
            items.clear();
        }
    }

    public void save(T item) {
        Optional.
                ofNullable(item).
                filter(t -> items.size() < MAX_ITEMS).
                map(items::add).
                orElseThrow(BufferOverflowException::new);
    }

}
