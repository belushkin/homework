package com.belushkin.lesson9;

import java.util.UUID;

public class ItemsService {
    public static void main(String[] args) {
        CloseableRepository<String> stringCloseableRepository = new CloseableRepository<>();

        try(stringCloseableRepository) {
            for (int i = 0; i < 11; i++) {
                stringCloseableRepository.save(UUID.randomUUID().toString());
            }
        }

    }
}
