package com.belushkin.hw9;

import com.belushkin.hw9.human.Human;
import com.belushkin.hw9.human.Man;
import com.belushkin.hw9.human.Woman;
import com.belushkin.hw9.pet.Fish;

import java.util.HashMap;
import java.util.Map;

public class Main {

    static {
        System.out.println(Main.class.getName() + " is loading");
    }

    {
        System.out.println(Main.class.getName() + " was created");
    }

    public static void main(String[] args) {

        FamilyService familyService = new FamilyService(new CollectionFamilyDao());
        FamilyController familyController = new FamilyController(familyService);

        // Getting all families and by index
        familyController.createNewFamily(
                new Man(),
                new Woman()
        );
        System.out.println(familyController.getAllFamilies());
        System.out.println(familyController.getFamilyByIndex(0));
        familyController.deleteFamily(0);
        System.out.println(familyController.getAllFamilies());

        // Removing families by index and by object
        Family family = familyController.createNewFamily(
                new Man(),
                new Woman()
        );
        System.out.println(familyController.getAllFamilies());
        familyController.deleteFamily(family);
        System.out.println(familyController.getAllFamilies());

        // Saving and displaying families
        familyController.saveFamily(family);
        familyController.displayAllFamilies();

        // Bigger than, less than
        System.out.println("Bigger than, less than");
        familyController.adoptChild(family, new Human());
        System.out.println(familyController.getFamiliesLessThan(2));
        System.out.println(familyController.getFamiliesLessThan(4));
        System.out.println(familyController.getFamiliesBiggerThan(2));
        System.out.println(familyController.getFamiliesBiggerThan(3));

        // Count and Delete children
        System.out.println(familyController.countFamiliesWithMemberNumber(3));
        familyController.deleteFamilyByIndex(0);
        System.out.println(familyController.countFamiliesWithMemberNumber(3));
        familyController.saveFamily(family);
        familyController.adoptChild(family,
                new Human("Foo", "Boo", 5),
                new Human("Foo", "Boo", 4)
        );
        familyController.deleteAllChildrenOlderThen(2);
        familyController.displayAllFamilies();

        // Count and pets
        System.out.println(familyController.count());
        System.out.println(familyController.getFamilyById(0));
        familyController.addPet(0, new Fish("Pe"));
        familyController.displayAllFamilies();
        System.out.println(familyController.getPets(0));
    }

}
