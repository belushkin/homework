package com.belushkin.hw9;

import com.belushkin.hw9.human.Human;
import com.belushkin.hw9.human.Man;
import com.belushkin.hw9.human.Woman;
import com.belushkin.hw9.pet.Pet;

import java.util.List;
import java.util.Set;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public Family getFamilyByIndex(int index) {
        return familyService.getFamilyByIndex(index);
    }

    public boolean deleteFamily(int index) {
        return familyService.deleteFamily(index);
    }

    public boolean deleteFamily(Family family) {
        return familyService.deleteFamily(family);
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int amount) {
        return familyService.getFamiliesBiggerThan(amount);
    }

    public List<Family> getFamiliesLessThan(int amount) {
        return familyService.getFamiliesLessThan(amount);
    }

    public int countFamiliesWithMemberNumber(int amount) {
        return familyService.countFamiliesWithMemberNumber(amount);
    }

    public Family createNewFamily(Man man, Woman woman) {
        return familyService.createNewFamily(man, woman);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamily(index);
    }

    public void bornChild(Family family, String maleName, String femaleName) {
        familyService.bornChild(family, maleName, femaleName);
    }

    public Family adoptChild(Family family, Human... child) {
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet... animal) {
        familyService.addPet(familyIndex, animal);
    }

}
