package com.belushkin.hw9.pet;

import java.util.Set;

public class RoboCat extends Pet implements FoulInterface {

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public RoboCat() {
        super();
    }

    public void respond() {
        if (getNickname() != null) {
            String message = "Hello host. I am " +
                    getNickname() +
                    ". I am robo cat";
            System.out.println(message);
        }
    }

    public void foul() {
        if (getNickname() != null) {
            System.out.println("Breaking all the time...");
        }
    }

}
