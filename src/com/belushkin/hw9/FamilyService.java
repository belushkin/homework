package com.belushkin.hw9;

import com.belushkin.hw9.human.Human;
import com.belushkin.hw9.human.Man;
import com.belushkin.hw9.human.Woman;
import com.belushkin.hw9.pet.Pet;

import java.util.*;

public class FamilyService {

    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public Family getFamilyByIndex(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public boolean deleteFamily(int index) {
        return familyDao.deleteFamily(index);
    }

    public boolean deleteFamily(Family family) {
        return familyDao.deleteFamily(family);
    }

    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

    public void displayAllFamilies() {
        for (Family allFamily : familyDao.getAllFamilies()) {
            System.out.println(allFamily.toString());
        }
    }

    public List<Family> getFamiliesBiggerThan(int amount) {
        List<Family> biggerFamilies = new ArrayList<>();

        for (Family allFamily : familyDao.getAllFamilies()) {
            if (allFamily.countFamily() > amount) {
                biggerFamilies.add(allFamily);
                System.out.println(allFamily.toString());
            }
        }
        return biggerFamilies;
    }

    public List<Family> getFamiliesLessThan(int amount) {
        List<Family> lessFamilies = new ArrayList<>();

        for (Family allFamily : familyDao.getAllFamilies()) {
            if (allFamily.countFamily() < amount) {
                lessFamilies.add(allFamily);
                System.out.println(allFamily.toString());
            }
        }
        return lessFamilies;
    }

    public int countFamiliesWithMemberNumber(int amount) {
        int count = 0;

        for (Family allFamily : familyDao.getAllFamilies()) {
            if (allFamily.countFamily() == amount) {
                count++;
            }
        }
        return count;
    }

    public Family createNewFamily(Man man, Woman woman) {
        Family family = new Family(
                man,
                woman,
                null
        );
        familyDao.saveFamily(family);
        return family;
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public void bornChild(Family family, String maleName, String femaleName) {
        Woman mother = family.getMother();

        Human child = mother.bornChild();
        if (child.getClass().getSimpleName().equals("Man")) {
            child.setName(maleName);
        } else {
            child.setName(femaleName);
        }
    }

    public Family adoptChild(Family family, Human... child) {
        for (Human human : child) {
            family.addChild(human);
        }
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        List<Family> families = familyDao.getAllFamilies();
        for (Family family : families) {
            family.getChildren().removeIf(child -> child.getYear() > age);
        }
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int familyIndex) {
        Family family = getFamilyByIndex(familyIndex);
        if (family != null) {
            return family.getPets();
        }
        return Collections.emptySet();
    }

    public void addPet(int familyIndex, Pet... animal) {
        Family family = getFamilyByIndex(familyIndex);
        if (family != null) {
            family.addPet(animal);
        }
    }
}
