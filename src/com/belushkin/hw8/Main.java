package com.belushkin.hw8;

import com.belushkin.hw8.human.Human;

import java.util.HashMap;
import java.util.Map;

public class Main {

    static {
        System.out.println(Main.class.getName() + " is loading");
    }

    {
        System.out.println(Main.class.getName() + " was created");
    }

    public static void main(String[] args) {

        for (int i = 0; i < 100000; i++) {

            Map<String, String> schedule = new HashMap< String,String>() {{
                put(DayOfWeek.Monday.name(), "walk in a park");
                put(DayOfWeek.Tuesday.name(), "go to school");
                put(DayOfWeek.Wednesday.name(), "watch a film");
                put(DayOfWeek.Thursday.name(), "have a rest");
                put(DayOfWeek.Friday.name(), "visit friends");
            }};

            Human human = new Human(
                    "name",
                    "surname",
                    1987,
                    99,
                    schedule);
        }

    }

}
