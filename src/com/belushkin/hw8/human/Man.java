package com.belushkin.hw8.human;

import java.util.Map;

public final class Man extends Human {

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Map<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man() {
        super();
    }

    @Override
    public void greetPet() {
        if (getFamily().getPet() != null && getFamily().getPet().getNickname() != null) {
            System.out.println("Welcome " + getFamily().getPet().getNickname());
        }
    }

    public String repairCar() {
        return "I am repairing car";
    }

}
