package com.belushkin.hw8.human;

import java.util.Map;
import java.util.Random;

public final class Woman extends Human implements HumanCreator {

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, Map<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman() {
        super();
    }

    @Override
    public void greetPet() {
        if (getFamily().getPet() != null && getFamily().getPet().getNickname() != null) {
            System.out.println("Nice to meet you " + getFamily().getPet().getNickname());
        }
    }

    public String makeup() {
        return "I am making up";
    }

    @Override
    public Human bornChild() {
        Random random = new Random();
        Human child;

        int sex = random.nextInt(1);
        String name = Names.values()[random.nextInt(Names.values().length)].name();
        String surname = getFamily().getFather().getSurname();
        int iq = (getFamily().getFather().getIq() + getFamily().getMother().getIq()) / 2;

        if (sex == 0) {
            child = new Man(name, surname,0, iq,null);
        } else {
            child = new Woman(name, surname,0, iq,null);
        }
        getFamily().addChild(child);
        return child;
    }
}
