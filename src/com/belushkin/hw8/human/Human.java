package com.belushkin.hw8.human;

import com.belushkin.hw8.Family;

import java.util.Map;
import java.util.Objects;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Map<String, String> schedule;
    private Family family;

    static {
        System.out.println(Human.class.getName() + " is loading");
    }

    {
        System.out.println(Human.class.getName() + " was created");
    }

    public Human(String name, String surname, int year) {

        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, Map<String, String> schedule) {

        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human() {

    }

    public int getIq() {
        return iq;
    }

    public String getSurname() {
        return surname;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    public void greetPet() {
        if (family.getPet() != null && family.getPet().getNickname() != null) {
            System.out.println("Hello " + family.getPet().getNickname());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;

        if (name == null && human.name != null) {
            return false;
        }
        if (surname == null && human.surname != null) {
            return false;
        }
        if (schedule == null && human.schedule != null) {
            return false;
        }

        return year == human.year
                && iq == human.iq &&
                (name == null || name.equals(human.name)) &&
                (surname == null || surname.equals(human.surname)) &&
                (schedule == null || schedule.equals(human.schedule));
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq, schedule);
    }

    @Override
    public String toString() {
        StringBuilder human = new StringBuilder(this.getClass().getSimpleName() + "{");
        if (name != null) {
            human.
                    append("name='").
                    append(name).
                    append("', ");
        }

        if (surname != null) {
            human.
                    append("surname='").
                    append(surname).
                    append("', ");
        }

        if (year > 0) {
            human.
                    append("year='").
                    append(year).
                    append("', ");
        }

        if (iq > 0) {
            human.
                    append("iq='").
                    append(iq).
                    append("', ");
        }

        if (schedule == null) {
            human.
                    append("}");

        } else {
            human.
                    append("schedule='").
                    append(schedule.toString()).
                    append("}");
        }

        if (human.charAt(human.length()-3) == ',') {
            human.deleteCharAt(human.length()-3);
            human.deleteCharAt(human.length()-2);
        }
        return human.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Garbage Collector, human: " + this);
    }

}
