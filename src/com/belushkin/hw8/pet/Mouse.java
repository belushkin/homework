package com.belushkin.hw8.pet;

import java.util.Set;

public class Mouse extends Pet {

    public void respond() {
    }

    public Mouse(String nickname) {
        super(nickname);
    }

    public Mouse(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Mouse() {
        super();
    }
}
