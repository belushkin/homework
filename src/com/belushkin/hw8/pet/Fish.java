package com.belushkin.hw8.pet;

import java.util.Set;

public class Fish extends Pet {

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Fish() {
        super();
    }

    public void respond() {
        if (getNickname() != null) {
            String message = "Hello host. I am " +
                    getNickname() +
                    ". I am fish";
            System.out.println(message);
        }
    }

}
