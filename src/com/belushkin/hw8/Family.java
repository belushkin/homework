package com.belushkin.hw8;

import com.belushkin.hw8.human.Human;
import com.belushkin.hw8.human.Man;
import com.belushkin.hw8.human.Woman;
import com.belushkin.hw8.pet.Pet;

import java.util.*;

public class Family {

    private Human father;
    private Human mother;
    private List<Human> children = new ArrayList<Human>();
    private Set<Pet> pets = new HashSet<Pet>();
    private int TRICK_COMPARE_LIMIT = 100;

    static {
        System.out.println(Family.class.getName() + " is loading");
    }

    {
        System.out.println(Family.class.getName() + " was created");
    }

    public Family(Man father, Woman mother, Pet pet, Human... children) {

        this.father = father;
        this.father.setFamily(this);

        this.mother = mother;
        this.mother.setFamily(this);

        this.pets.add(pet);
        for (Human child : children) {
            child.setFamily(this);
            this.children.add(child);
        }
    }

    public void addPet(Pet pet) {

        pets.add(pet);
    }

    public void addChild(Human child) {

        child.setFamily(this);
        children.add(child);
    }

    public boolean deleteChild(Human value) {

        return children.removeIf(value::equals);
    }

    public boolean deleteChild(int index) {
        if (index <= 0 || index > children.size()) {
            return false;
        }
        children.remove(index-1);
        return true;
    }

    public Pet getPet() {
        if (pets.size() == 0) {
            return null;
        }
        return (Pet) pets.toArray()[0];
    }

    public Human getFather() {
        return father;
    }

    public Human getMother() {
        return mother;
    }

    public int countFamily() {
        return children.size() + 2;
    }

    public void describePet() {
        if (getPet() != null && getPet().getSpecies() != null) {
            StringBuilder description = new StringBuilder("I have a ").
                    append(getPet().getSpecies());

            if (getPet().getAge() > 0) {
                description.
                        append(", it is ").
                        append(getPet().getAge()).
                        append(" years");
            }

            if (getPet().getTrickLevel() > 0) {
                description.
                        append(", it is ").
                        append(getPet().getTrickLevelAsString());
            }
            System.out.println(description.toString());
        }
    }

    public boolean feedPet(boolean isTimeToFeed) {
        if (getPet() == null || getPet().getNickname() == null) {
            return false;
        }

        if (isTimeToFeed) {
            System.out.println("Hm... Let's feed " + getPet().getNickname());
            getPet().eat();
            return true;
        } else {
            Random random = new Random();
            int trickCompareNumber = random.nextInt(TRICK_COMPARE_LIMIT);
            if (getPet().getTrickLevel() > trickCompareNumber) {
                System.out.println("Hm... Let's feed " + getPet().getNickname());
                getPet().eat();
                return true;
            }
        }

        System.out.println("I think " + getPet().getNickname() + " is not hungry.");
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;

        if (father == null && family.father != null) {
            return false;
        }
        if (mother == null && family.mother != null) {
            return false;
        }
        if (pets == null && family.pets != null) {
            return false;
        }
        if (children == null && family.children != null) {
            return false;
        }

        return TRICK_COMPARE_LIMIT == family.TRICK_COMPARE_LIMIT &&
                (father == null || father.equals(family.father)) &&
                (mother == null || mother.equals(family.mother)) &&
                (children == null || children.equals(family.children)) &&
                (pets == null || pets.equals(family.pets));
    }

    @Override
    public int hashCode() {
        return Objects.hash(father, mother, children, pets, TRICK_COMPARE_LIMIT);
    }

    @Override
    public String toString() {
        StringBuilder family = new StringBuilder("Family{");
        family.
                append("father='").
                append(father).
                append("', ");

        family.
                append("mother='").
                append(mother).
                append("', ");

        if (children != null && children.size() > 0) {
            family.
                    append("children='").
                    append(children.toString()).
                    append("', ");
        }

        if (getPet() != null) {
            family.
                    append("pet='").
                    append(getPet()).
                    append("}");

        } else {
            family.
                    append("}");
        }

        if (family.charAt(family.length()-3) == ',') {
            family.deleteCharAt(family.length()-3);
            family.deleteCharAt(family.length()-2);
        }
        return family.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Garbage Collector, family: " + this);
    }

}
