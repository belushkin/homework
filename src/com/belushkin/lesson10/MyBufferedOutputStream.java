package com.belushkin.lesson10;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MyBufferedOutputStream extends OutputStream {

    private List<byte[]> bytes = new ArrayList<byte[]>();
    private final static int DEFAULT_BUFFER_SIZE = 36;
    private byte[] currentByteArray = new byte[DEFAULT_BUFFER_SIZE];
    private int counter = 0;

    public MyBufferedOutputStream() {
        this.bytes.add(currentByteArray);
    }

    public List<byte[]> getBytes() {
        return bytes;
    }

    public void write(byte b) {
        if (counter < DEFAULT_BUFFER_SIZE) {
            currentByteArray[counter++] = b;
        } else {
            bytes.add(currentByteArray);
        }
    }

    public void write(byte[] bytes) throws IOException {

    }

    @Override
    public void write(int i) throws IOException {

    }

}
