package com.belushkin.lesson10;

import java.io.ByteArrayInputStream;

public class App00 {

    public static void main(String[] args) throws InterruptedException {
        byte[] bytes = new byte[]{65,97};
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        for(int read = byteArrayInputStream.read(); read != -1;) {
            System.out.println((char)read);
            read = byteArrayInputStream.read();
        }

        for (byte b = 10; b > -127; b--) {
            System.out.println((char)b);
            System.out.println(b);
            Thread.sleep(2000);
        }
    }
}
