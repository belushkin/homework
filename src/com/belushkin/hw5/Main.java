package com.belushkin.hw5;

public class Main {

    static {
        System.out.println(Main.class.getName() + " is loading");
    }

    {
        System.out.println(Main.class.getName() + " was created");
    }

    public static void main(String[] args) {
        firstFamily();
        secondFamily();

        Human h1 = new Human(
                "test1",
                "test1",
                2006
        );

        Human h2 = new Human(
                "test2",
                "test2",
                2007
        );
        Human h3 = new Human(
                "test3",
                "test3",
                2008
        );

        Family family = new Family(
                new Human(),
                new Human(),
                null,
                h1, h2, h3
        );

        family.addChild(new Human());

        System.out.println(family.countFamily());
        System.out.println(family);
        family.deleteChild(h3);
        System.out.println(family.countFamily());
        System.out.println(family);

    }

    static void firstFamily() {
        Human father = new Human("Nat", "Wolf", 1984);
        Human mother = new Human("Margaret", "Qualley", 1985);

        Human son = new Human(
                "LaKeith",
                "Stanfield",
                2006
        );

        String[][] schedule = {
                {"1", "walk in a park"},
                {"2", "go to school"},
                {"3", "watch a film"},
                {"4", "have a rest"},
                {"5", "visit friends"},
        };
        Human daughter = new Human(
                "Jullia",
                "Wallpaper",
                2006,
                89,
                schedule
        );

        String[] habits = {"eat", "sleep", "walk"};
        Pet dog = new Pet(
                "dog",
                "batman",
                3,
                56,
                habits
        );

        Family family = new Family(
                father,
                mother,
                dog,
                son,
                daughter
        );

        System.out.println(family);
        family.callMethods();
    }

    static void secondFamily() {
        Human father = new Human("Shea", "Whigham", 1983);
        Human mother = new Human();

        Pet cat = new Pet("cat", "hollywood");
        String[][] sonSchedule = {
                {"1", "walk on the street"},
                {"2", "go to shop"},
                {"3", "watch a cartoon"},
                {"4", "work"},
                {"5", "visit grandma"},
        };
        Human son = new Human(
                "Timothy",
                "Olyphant",
                2005,
                88,
                sonSchedule
        );

        String[][] daughterSchedule = {
                {"1", "run"},
                {"2", "go to cinema"},
                {"3", "go to spa"},
                {"4", "program"},
                {"5", "drink beer"},
        };
        Human daughter = new Human(
                "Julia",
                "Butters",
                2003,
                86,
                daughterSchedule
        );

        Family family = new Family(
                father,
                mother,
                cat,
                son,
                daughter
        );
        System.out.println(family);
        family.callMethods();
    }
}
