package com.belushkin.hw5;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;
    private Family family;

    static {
        System.out.println(Human.class.getName() + " is loading");
    }

    {
        System.out.println(Human.class.getName() + " was created");
    }

    public Human(String name, String surname, int year) {

        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, String[][] schedule) {

        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human() {

    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                iq == human.iq &&
                name.equals(human.name) &&
                surname.equals(human.surname) &&
                Arrays.equals(schedule, human.schedule) &&
                family.equals(human.family);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq, family);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder human = new StringBuilder("Human{");
        if (name != null) {
            human.
                    append("name='").
                    append(name).
                    append("', ");
        }

        if (surname != null) {
            human.
                    append("surname='").
                    append(surname).
                    append("', ");
        }

        if (year > 0) {
            human.
                    append("year='").
                    append(year).
                    append("', ");
        }

        if (iq > 0) {
            human.
                    append("iq='").
                    append(iq).
                    append("', ");
        }

        if (schedule == null) {
            human.
                    append("}");

        } else {
            human.
                    append("schedule='").
                    append(Arrays.deepToString(schedule));
        }

        if (human.charAt(human.length()-3) == ',') {
            human.deleteCharAt(human.length()-3);
            human.deleteCharAt(human.length()-2);
        }
        return human.toString();
    }
}
