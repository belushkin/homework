package com.belushkin.hw5;

import java.util.Arrays;
import java.util.Objects;

public class Pet {

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    private final int TRICKNESS_LEVEL = 50;

    static {
        System.out.println(Pet.class.getName() + " is loading");
    }

    {
        System.out.println(Pet.class.getName() + " was created");
    }

    public Pet(String species, String nickname){

        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits){

        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(){

    }

    public String getNickname() {
        return nickname;
    }

    public String getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String getTrickLevelAsString() {
        return getTrickLevel() > TRICKNESS_LEVEL ? "tricky" : "not tricky";
    }

    public void eat() {
        System.out.println("I am eating!");
    }

    public void respond() {
        if (getNickname() != null) {
            StringBuilder message = new StringBuilder("Hello host. I am ").
                    append(getNickname()).
                    append(". I am boring");
            System.out.println(message.toString());
        }
    }

    public void foul() {
        if (getNickname() != null) {
            System.out.println("Cover all tracks well is needed...");
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                TRICKNESS_LEVEL == pet.TRICKNESS_LEVEL &&
                species.equals(pet.species) &&
                nickname.equals(pet.nickname) &&
                Arrays.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname, age, trickLevel, TRICKNESS_LEVEL);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }

    @Override
    public String toString() {
        if (species == null) {
            return "{}";
        }

        StringBuilder pet = new StringBuilder(species + "{");
        if (nickname != null) {
            pet.
                    append("nickname='").
                    append(nickname).
                    append("', ");
        }

        if (age > 0) {
            pet.
                    append("age='").
                    append(age).
                    append("', ");
        }

        if (trickLevel > 0) {
            pet.
                    append("trickLevel='").
                    append(trickLevel).
                    append("', ");
        }

        if (habits != null) {
            pet.
                    append("habits='").
                    append(Arrays.toString(habits)).
                    append("}");
        } else {
            pet.
                    append("}");
        }

        if (pet.charAt(pet.length()-3) == ',') {
            pet.deleteCharAt(pet.length()-3);
            pet.deleteCharAt(pet.length()-2);
        }
        return pet.toString();
    }
}
