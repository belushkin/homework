package com.belushkin.hw1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Runner
{

    private static int[] nums = new int[5];

    private static int count = 0;

    public static void main(String[] args)
    {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter your name %username%= ");
        String name = scanner.nextLine();

        System.out.println("Let the game begin!");

        String[] events = {
                "Ned Loses His Head",
                "Dragons Return To The World",
                "The War Of The Five Kings",
                "The Red Wedding",
                "The Purple Wedding",
                "The Battle Of Castle Black",
                "Jon Is Resurrected",
                "The Battle Of The Bastards",
                "Cersei Destroys The Sept Of Baelor",
                "The Battle Of Winterfell",
                "Daenerys' Reign Ends"
        };
        int randomNum = random.nextInt(events.length);
        int eventNum = randomNum + 1;

        System.out.println("In which chronological order next moment has been presented in " +
                "the saga of Game of Thrones: " + events[randomNum]);

        int num = 0;

        do {
            String input = scanner.nextLine();

            if (input.matches("\\d+")) {
                num = Integer.parseInt(input);
            } else {
                System.out.println("Integers only, please!");
                continue;
            }

            if (num < eventNum) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (num > eventNum) {
                System.out.println("Your number is too big. Please, try again.");
            }
            addElementToArray(num);
        }
        while(eventNum != num);

        System.out.printf("Congratulations, %s!\n", name);
        System.out.println("Your numbers:");

        Arrays.sort(nums);
        for (int value : nums) {
            if (value != 0) {
                System.out.print(value + " ");
            }
        }
    }

    private static void addElementToArray(int value)
    {
        if (nums.length*0.8 < count) {
            extend();
        }
        nums[count++] = value;
    }

    private static void extend()
    {
        int[] arrayCopy = new int[nums.length * 2];
        System.arraycopy(nums, 0, arrayCopy, 0, nums.length);
        nums = arrayCopy;
    }

}
