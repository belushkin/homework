package com.belushkin.hw2;

import java.util.Random;

public class Grid
{

    Character[][] grid = {
            {'-', '-', '-', '-', '-'},
            {'-', '-', '-', '-', '-'},
            {'-', '-', '-', '-', '-'},
            {'-', '-', '-', '-', '-'},
            {'-', '-', '-', '-', '-'},
    };

    Character[][] target = {
            {'-', '-', '-', '-', '-'},
            {'-', '-', '-', '-', '-'},
            {'-', '-', '-', '-', '-'},
            {'-', '-', '-', '-', '-'},
            {'-', '-', '-', '-', '-'},
    };

    int targetSize = 3;
    int gridSpace = 2;

    int targetX;
    int targetY;

    boolean isVertical;

    public Grid()
    {
        Random random = new Random();

        this.isVertical = random.nextBoolean();

        if (this.isVertical) {
            this.targetX = random.nextInt(getLength());
            this.targetY = random.nextInt(this.gridSpace);

            setVerticalTarget();
        } else {
            this.targetX = random.nextInt(this.gridSpace);
            this.targetY = random.nextInt(getHeight());

            setHorizontalTarget();
        }
    }

    int getLength()
    {
        return grid.length;
    }

    int getHeight()
    {
        return grid[0].length;
    }

    void showGrid()
    {
        //Printing first row with numbers
        for (int i = 0; i < grid.length+1; i++) {
            System.out.print(i + " | ");
        }

        // Printing grid
        System.out.println();
        for (int i = 0; i < grid.length; i++) {
            System.out.print(i+1 + " | ");
            for (int j = 0; j < grid.length; j++) {
                System.out.print(grid[i][j] + " | ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void updateGrid(int x, int y)
    {
        if (target[x][y] == '?' || target[x][y] == 'X') {
            grid[x][y]   = 'X';
            target[x][y] = 'X';
        } else {
            grid[x][y] = '*';
        }
    }

    boolean isAlive()
    {
        boolean play = false;

        for (int i = 0; i < targetSize; i++) {

            int x = (isVertical) ? targetX : targetX + i;
            int y = (isVertical) ? targetY + i : targetY;

            if (target[x][y] == '?') {
                play = true;
                break;
            }
        }
        return play;
    }

    void setVerticalTarget()
    {
        for (int i = 0; i < targetSize; i++) {
            target[targetX][targetY + i] = '?';
        }
    }

    void setHorizontalTarget()
    {
        for (int i = 0; i < targetSize; i++) {
            target[targetX + i][targetY] = '?';
        }
    }

}
