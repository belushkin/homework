package com.belushkin.hw2;

import java.util.Scanner;

public class Input
{

    private Scanner kb;

    private int inputInt;

    private String source;

    private int min;

    private int max;

    public Input(String source, int min, int max)
    {
        this.kb = new Scanner(System.in);
        this.source = source;
        this.min = min;
        this.max = max;
    }

    public int getInputInt()
    {
        do {
            System.out.println("Select " + source + ": ");
            String input = kb.nextLine();

            if (input.matches("\\d+")) {
                if (Integer.parseInt(input) > max || Integer.parseInt(input) < min) {
                    System.out.println("Such " + source + " does not exist in the field!\n");
                } else {
                    inputInt = Integer.parseInt(input);
                }
            } else {
                System.out.println("Integers only, please!\n");
            }
        }
        while (inputInt == 0);
        return inputInt;
    }

}