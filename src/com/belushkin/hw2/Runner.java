package com.belushkin.hw2;

public class Runner
{

    public static void main(String[] args)
    {

        Grid grid = new Grid();

        System.out.println("All set. Get ready to rumble!\n");

        do {
            grid.showGrid();

            // Working with user input row
            Input rowInput = new Input("row", 1, grid.getHeight());
            int row = rowInput.getInputInt();

            // Working with user input column
            Input columnInput = new Input("column", 1, grid.getLength());
            int column = columnInput.getInputInt();

            // Work with grid
            grid.updateGrid(row-1, column-1);
        }
        while (grid.isAlive());

        // show grid one more time
        grid.showGrid();
        System.out.println("You have won!");
    }

}
